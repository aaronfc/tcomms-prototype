package es.com.aaron.tcomms.examples.providers.custom_user_data;


import es.com.aaron.tcomms.interfaces.data_providers.DataProvider;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import java.util.*;

/**
 * CustomUserData example data provider.
 *
 * This is a non-working data provider with the only purpose os serving as an initial template for new data providers.
 */
public class CustomUserDataDataProvider implements DataProvider {

	@Override
	public Set<String> getAllParametersKeys() {
		Set<String> paramsSet = new HashSet<String>();
		paramsSet.addAll(Arrays.asList("user_id", "username", "email", "city", "age", "gender"));
		return paramsSet;
	}

	@Override
	public Set<String> getInputParameterKeys() {
		Set<String> required = new HashSet<String>();
		required.add("user_id");
		return required;
	}

	@Override
	public ParamsBag getRemoteParameters(ParamsBag inputParameters, Set<String> requiredParameters) {
		String userId = inputParameters.get("user_id").toString();

		ParamsBag outParams = new ParamsBag();

		// Gather information making use of `user_id` content.
		// Examples: Request to remote WS / Request remote database / Read data from files
		Map<String, String> remoteValues = new HashMap<String, String>();

		// Set (at least) the parameters that are specified in the "requiredParameters" set.
		// You can of course set more than required.
		// Something like:
		outParams.put("username", remoteValues.get("user"));
		outParams.put("email", remoteValues.get("email"));
		// ...
		outParams.put("gender", remoteValues.get("gender"));

		// Return values
		return outParams;
	}

	@Override
	public String getPrefix() {
		return "CustomUserExample";
	}
}
