package es.com.aaron.tcomms.examples.senders.twitter;

import es.com.aaron.tcomms.interfaces.senders.Sender;
import es.com.aaron.tcomms.interfaces.senders.SenderFields;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * TwitterSender as an example of a TComms sender.
 * This class will post communications
 */
public class TwitterSender implements Sender {
	@Override
	public Set<String> getRequiredFields() {
		return new HashSet<String>(Arrays.asList("username", "body"));
	}

	@Override
	public void send(SenderFields senderFields) {
		// TODO Implement this!
	}
}
