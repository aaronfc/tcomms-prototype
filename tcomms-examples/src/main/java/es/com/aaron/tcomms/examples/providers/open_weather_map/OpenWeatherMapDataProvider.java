package es.com.aaron.tcomms.examples.providers.open_weather_map;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import es.com.aaron.tcomms.interfaces.data_providers.DataProvider;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * OpenWeatherMap data provider.
 */
public class OpenWeatherMapDataProvider implements DataProvider {

	private static JsonParser jsonParser = new JsonParser();

	private final String WS_URL = "http://api.openweathermap.org/data/2.5/weather?units=metric&q=";

	@Override
	public Set<String> getAllParametersKeys() {
		Set<String> paramsSet = new HashSet<String>();
		paramsSet.addAll(Arrays.asList("city_name", "temperature", "temperature_min", "temperature_max", "pressure", "humidity", "cloudiness"));
		return paramsSet;
	}

	@Override
	public Set<String> getInputParameterKeys() {
		Set<String> required = new HashSet<String>();
		required.add("city_name");
		return required;
	}

	@Override
	public ParamsBag getRemoteParameters(ParamsBag inputParameters, Set<String> requiredParameters) {
		String cityName = inputParameters.get("city_name").toString();

		ParamsBag outParams = new ParamsBag();

		// Make http request
		String jsonOutput = getContent(WS_URL + cityName);

		// Parse JSON output. Empty params set will be returned on parsing error.
		try {
			JsonObject object = jsonParser.parse(jsonOutput).getAsJsonObject();
			// Load main values
			JsonObject mainValues = object.getAsJsonObject("main");
			outParams.put("temperature", mainValues.getAsJsonPrimitive("temp").getAsString());
			outParams.put("temperature_min", mainValues.getAsJsonPrimitive("temp_min").getAsString());
			outParams.put("temperature_max", mainValues.getAsJsonPrimitive("temp_max").getAsString());
			outParams.put("pressure", mainValues.getAsJsonPrimitive("pressure").getAsString());
			outParams.put("humidity", mainValues.getAsJsonPrimitive("humidity").getAsString());
			// Load cloudiness
			JsonObject cloudsValues = object.getAsJsonObject("clouds");
			outParams.put("cloudiness", cloudsValues.getAsJsonPrimitive("all").getAsString());
		} catch (JsonSyntaxException ignored) {}

		return outParams;
	}

	@Override
	public String getPrefix() {
		return "OpenWeatherMap";
	}


	/**
	 * Get content from a url. Will return empty string on error.
	 * @param urlAsString Url to read as a String
	 * @return String output
	 */
	private String getContent(String urlAsString) {
		String output = "";
		try {
			URL url;
			URLConnection urlConn;
			DataInputStream dis;

			url = new URL(urlAsString);

			urlConn = url.openConnection();
			urlConn.setDoInput(true);
			urlConn.setUseCaches(false);

			dis = new DataInputStream(urlConn.getInputStream());
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((dis)));
			String s;

			while ((s = bufferedReader.readLine()) != null) {
				output += s;
			}
			dis.close();
		} catch (MalformedURLException ignored) {
		} catch (IOException ignored) {}
		return output;
	}
}
