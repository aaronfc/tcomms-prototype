package es.com.aaron.tcomms.interfaces.senders;

import java.util.HashMap;

/**
 * SenderFields map.
 *
 * This is a map of fields expected by a Sender to assure the delivery of a message to a receiver.
 */
public class SenderFields extends HashMap<String, String> {}
