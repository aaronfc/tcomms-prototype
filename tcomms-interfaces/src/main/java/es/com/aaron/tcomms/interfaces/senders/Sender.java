package es.com.aaron.tcomms.interfaces.senders;

import java.util.Set;

/**
 * Sender interface.
 */
public interface Sender {
	/**
	 * Generate the list of required parameter names by this sender.
	 * @return Required parameters
	 */
	public Set<String> getRequiredFields();

	/**
	 * Actual delivery of the communication.
	 */
	public void send(SenderFields senderFields);
}
