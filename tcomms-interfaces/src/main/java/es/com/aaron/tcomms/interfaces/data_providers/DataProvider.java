package es.com.aaron.tcomms.interfaces.data_providers;

import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import java.util.Set;

/**
 * DataProvider interface.
 *
 * Classes implementing this interface will be used to calculate a required set of parameters (given their keys)
 * starting from a initial bag of parameters.
 */
public interface DataProvider {
	/**
	 * All parameter keys that can generate the data provider.
	 * @return Set of all parameter keys that can be generated.
	 */
	public Set<String> getAllParametersKeys();

	/**
	 * Actual generation of the output parameters bag containing at least the required set of parameter keys specified
	 * making use of the input parameters bag.
	 * @param inputParameters Input parameters bag
	 * @param requiredParameterKeys Minimum required set of parameter keys
	 * @return Output parameters bag
	 */
	public ParamsBag getRemoteParameters(ParamsBag inputParameters, Set<String> requiredParameterKeys);

	/**
	 * Get expected input parameters.
	 * @return Input parameter keys expected
	 */
	public Set<String> getInputParameterKeys();

	/**
	 * Unique prefix to identify this data provider.
	 * @return Unique prefix identifier string
	 */
	public String getPrefix();
}
