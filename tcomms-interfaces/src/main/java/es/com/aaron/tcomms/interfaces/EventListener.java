package es.com.aaron.tcomms.interfaces;

/**
 * Event listener interface.
 */
public interface EventListener {

	public Boolean handleEvent(String eventName, String eventInput);
}
