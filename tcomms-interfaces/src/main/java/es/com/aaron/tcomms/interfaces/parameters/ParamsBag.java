package es.com.aaron.tcomms.interfaces.parameters;

import com.google.common.collect.Lists;

import java.util.*;
import java.util.regex.Pattern;

/**
 * ParamsBag class.
 *
 * This class is a container of parameters stored as a Map<String, Object>.
 * It is able to use kind of smart keys that define a path for nested maps using the period character as separator.
 * For example, a key "this.is.smart" will reference a first level element "this" that is a map containing an element
 * with "is" key, which in turn has a "smart" element, that might be or not a map or an object.
 */
public class ParamsBag extends HashMap<String, Object> {

	private final String SEPARATOR = ".";

	public ParamsBag() {}

	public ParamsBag(Map<? extends String, ?> map) {
		super(map);
	}

	/**
	 * Check if a set of keys (in form of strings) is contained in the parameters bag.
	 * @param keySet Set of parameter keys to check
	 * @return Whether all keys are contained or not
	 */
	public boolean containsKeySet(Set<String> keySet) {
		for(String key : keySet) {
			if (!containsKey(key)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check if a given key is contained in the parameters bag.
	 * @param key Key to check
	 * @return Whether if it exists or not
	 */
	public boolean containsKey(String key) {
		return recursiveMapContainsKey(this, Arrays.asList(key.split(Pattern.quote(SEPARATOR))));
	}

	/**
	 * Recursive method to check if a key is contained.
	 * @param map Map to check
	 * @param subKeys List of subkeys
	 * @return Whether if it exists or not
	 */
	private boolean recursiveMapContainsKey(Map<String, Object> map, List<String> subKeys) {
		String candidateKey = subKeys.get(0);
		List<String> remainingSubKeys = new ArrayList<String>(subKeys);
		remainingSubKeys.remove(0);
		if (map.containsKey(candidateKey)) {
			if (remainingSubKeys.isEmpty()) {
				return true;
			}
			Object candidate = map.get(candidateKey);
			if (candidate instanceof Map) {
				return recursiveMapContainsKey((Map<String, Object>) candidate, remainingSubKeys);
			}
		}
		return false;
	}

	/**
	 * Method to add a new parameter. Smart keys are accepted.
	 * @param key Key to add
	 * @param value Value
	 */
	public void put(String key, String value) {
		// Old behaviour
		if (!key.contains(SEPARATOR)) {
			super.put(key, value);
			return;
		}

		// Smart key detected
		List<String> pieces = Lists.newArrayList(key.split(Pattern.quote(SEPARATOR)));
		String name = pieces.remove(pieces.size()-1);
		Map<String, Object> node = this;
		for (String piece : pieces) {
			if (!node.containsKey(piece)) {
				node.put(piece, new HashMap<String, Object>());
			}
			node = (HashMap<String, Object>) node.get(piece);
		}
		node.put(name, value);
	}

	/**
	 * Smart keys get method. This will handle keys containing the period character.
	 * @param key
	 * @return Matching element or null
	 */
	public Object get(String key) {
		if (!key.contains(SEPARATOR)) {
			return super.get(key);
		}

		// Smart key detected
		List<String> pieces = Lists.newArrayList(key.split(Pattern.quote(SEPARATOR)));
		String name = pieces.remove(pieces.size()-1);
		Map<String, Object> node = this;
		for (String piece : pieces) {
			if (!node.containsKey(piece)) {
				return null;
			}
			node = (HashMap<String, Object>) node.get(piece);
		}
		return node.get(name);
	}
}
