package es.com.aaron.tcomms.platform.bootstrap;

import java.lang.String;

/**
 * TCommsClient for Java.
 *
 * TODO Untested!
 *
 * @author Aaron Fas <yo@aaron.com.es>
 */
public class TCommsClient {

	private final String USER_AGENT = "Mozilla/5.0";

	private String listenerUrl;

	/**
	 * Constructor receiving only the listener URL. For example: localhost:8888/
	 * @param listenerUrl
	 */
	public TCommsClient(String listenerUrl) {
		this.listenerUrl = listenerUrl;
	}

	/**
	 * Send new communication event to the remote TComms.
	 * @param eventName
	 * @param parameters
	 */
	public onEvent(String eventName, Properties parameters) {
		String paramsAsJson = toJson(parameters);
		sendPost(this.listenerUrl, String.format("eventName=%s&params=%s", eventName, paramsAsJson));
	}

	/**
	 * Private function to convert Properties to JSON.
	 * @param properties
	 * @return
	 */
	private String toJson(Properties properties) {
		StringBuilder builder = new StringBuilder() ;
		builder.append('{');
		if (properties != null) {
			Enumeration keys = properties.keys();
			while (keys.hasMoreElements()) {
				String key = (String) keys.nextElement();
				String value = (String) properties.get(key);
				builder.append('"').append(key).append('"');
				builder.append(':');
				builder.append('"').append(value).append('"').append(',');
			}
			builder.deleteCharAt(builder.length() - 1);
		}
		builder.append('}');
		return builder.toString();
	}

	/**
	 * Private function to make the POST request.
	 * @param url
	 * @param urlParameters
	 */
	private void sendPost(String url, String urlParameters) {
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		// Request header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		// Send POST request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		// Get response, we might want to ignore this
		int responseCode = con.getResponseCode();

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//String result = response.toString();
	}
}