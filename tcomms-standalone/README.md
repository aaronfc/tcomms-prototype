TComms (tcomms-standalone)
==========================

The `tcomms-standalone` is a module of the TComms project that provides a boxed instance of the TComms with very little configurations required. 

Version
----
1.0

Step-by-step
------------
- Either generate the JAR yourself from the source code or download an already assembled one.
- Configure your `persistence.xml`
- Configure your `tcomms.properties` file (optional)
- Put all files (`tcomms-standalone-*.jar`, `persistence.xml` and `tcomms.properties`) in the same folder.
- Run: `java -jar tcomms-standalone-*.jar`
- That's it!

Configuring persistence.xml
---------------------------
You have to create a persistence.xml file (you can use the following one as a template).
The following sample is a valid configuration file for a MySQL database.

    <?xml version="1.0" encoding="UTF-8"?>
    <persistence version="2.0" xmlns="http://java.sun.com/xml/ns/persistence"
                 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                 xsi:schemaLocation="http://java.sun.com/xml/ns/persistence
    http://java.sun.com/xml/ns/persistence/persistence_2_0.xsd">
    
        <persistence-unit name="mainPU">
            <exclude-unlisted-classes>false</exclude-unlisted-classes>
            <properties>
    
                <property name="javax.persistence.jdbc.driver" value="com.mysql.jdbc.Driver"/>
                <property name="javax.persistence.jdbc.user" value="{MYSQL_USER_HERE}"/>
                <property name="javax.persistence.jdbc.password" value="{MYSQL_PASSWORD_HERE}"/>
                <property name="javax.persistence.jdbc.url" value="jdbc:mysql://{MYSQL_HOST_HERE}/{MYSQL_DATABASE_NAME_HERE}"/>
            </properties>
        </persistence-unit>
    </persistence>

Configuring tcomms.properties file
----------------------------------
If you do not setup any `tcomms.properties` file, you will have a REST webservice running on port `8888`.
Administration API will be located under the route `/api`
And the event listener will be up and ready to process any new coming events under `/`
Here you have the default tcomms.properties used, you can define any of this properties and they will override the default ones.

    # Servlet configuration
    tcomms.servlet.port = 8888
    # Admin API
    tcomms.servlet.admin.api.enabled = true
    # Main listener
    tcomms.servlet.listener.enabled = true


So ... What now?
--------------------
After executing the `tcomms-standalone-*.jar` you will have a tcomms instance up and running on your machine.
Check the documentation on TComms parent module to see some examples usages.
You can find TCommsClients for some programming languages under the `sample-clients` folder in the root of the project.


API example queries:
-----------
Get all communications for a given "event_name":

    curl localhost:8888/api/communications/event/my_event


Create a communication:

    curl -X POST localhost:8888/api/communications -d '{"id": 1, "title": "My title", "description": "Ny description", "conditions": null, "text": "My text", "eventName": "my_event", "sender": "the_sender"}' -H "Content-Type: application/json"


Main entry-point example requests:
-----------
Ping main entry-point:

    curl localhost:8888/


Example event reception using HTTPie (`sudo pip install httpie`):

    http --json -v POST localhost:8888/ eventName=test_event params="{}"
