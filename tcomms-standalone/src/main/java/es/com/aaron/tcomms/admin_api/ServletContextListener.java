package es.com.aaron.tcomms.admin_api;

import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import es.com.aaron.tcomms.admin_api.di.ServletsModule;
import es.com.aaron.tcomms.platform.bootstrap.MainBootstrap;

/**
 * ServletContext listener.
 * This class is the one used as context listener.
 * We generate the injector in two-steps to allow the ServletsModule access configuration properties.
 */
public class ServletContextListener extends GuiceServletContextListener {
	@Override
	protected Injector getInjector() {
		// Create main injector
		Injector mainInjector = MainBootstrap.getOrInitiateInjector();
		// Instantiate ServletModule making use of main injector.
		ServletsModule servletModule = mainInjector.getInstance(ServletsModule.class);
		// Return new child injector
		return mainInjector.createChildInjector(servletModule);
	}
}