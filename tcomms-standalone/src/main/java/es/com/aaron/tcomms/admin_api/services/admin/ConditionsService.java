package es.com.aaron.tcomms.admin_api.services.admin;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.models.Condition;
import es.com.aaron.tcomms.core.models.daos.ConditionDAO;

import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Conditions service API.
 *
 * The purpose of this class is to handle all the operations related to the Conditions management.
 */
@Path("/api/conditions")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ConditionsService {
	ConditionDAO conditionsDao;

	@Inject
	ConditionsService(ConditionDAO conditionsDao) {
		this.conditionsDao = conditionsDao;
	}

	@GET @Path("/all")
	public List<Condition> getAll() {
		return conditionsDao.findAll();
	}

	@GET @Path("{id}")
	public Condition get(@PathParam("id") Long id) {
		return conditionsDao.find(id);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Condition create(Condition condition) {
		condition = conditionsDao.insert(condition);
		return condition;
	}

	@PUT @Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Condition update(Condition condition) {
		condition = conditionsDao.update(condition);
		return condition;
	}

	@DELETE @Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void delete(@PathParam("id") Long id) {
		Condition condition = conditionsDao.find(id);
		if (condition != null) {
			conditionsDao.delete(condition);
		}
	}


}
