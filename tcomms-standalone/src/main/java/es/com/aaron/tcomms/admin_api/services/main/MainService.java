package es.com.aaron.tcomms.admin_api.services.main;

import com.google.inject.Inject;
import es.com.aaron.tcomms.interfaces.EventListener;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * MainService class.
 *
 * This service is in charge of handling events coming through a WS and delegating them to the events listener.
 */
@Path("/")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MainService {

	private final EventListener eventListener;

	@Inject
	public MainService(EventListener eventListener) {
		this.eventListener = eventListener;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String handleEvent(String eventName, String paramsJson) {
		// TODO Fix this method to return Boolean
		// Exception: Body writer for Boolean as a application/json not found.
		return eventListener.handleEvent(eventName, paramsJson).toString();
	}
}
