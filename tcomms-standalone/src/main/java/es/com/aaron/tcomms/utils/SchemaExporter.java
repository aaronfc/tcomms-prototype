package es.com.aaron.tcomms.utils;

import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.MySQL5Dialect;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.reflections.Reflections;

import javax.persistence.Entity;
import java.util.Set;

/**
 * SchemaExporter class.
 */
public class SchemaExporter {
	public static void main(String[] args) {
		System.out.println("Starting schema export");
		Configuration cfg = new Configuration();
		cfg.setProperty("hibernate.hbm2ddl.auto", "create");

		Reflections reflections = new Reflections("es.com.aaron.tcomms");
		Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(Entity.class);

		for (Class<?> class1 : typesAnnotatedWith) {
			cfg.addAnnotatedClass(class1);
		}


		cfg.setProperty("hibernate.dialect", MySQL5Dialect.class.getCanonicalName());
		SchemaExport export = new SchemaExport(cfg);
		export.setDelimiter(";");
		export.setOutputFile("schema.sql");
		export.setFormat(true);
		export.execute(true, false, false, false);
	}
}
