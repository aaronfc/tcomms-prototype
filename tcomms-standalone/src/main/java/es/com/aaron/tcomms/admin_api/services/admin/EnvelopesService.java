package es.com.aaron.tcomms.admin_api.services.admin;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.models.Envelope;
import es.com.aaron.tcomms.core.models.daos.EnvelopeDAO;
import es.com.aaron.tcomms.interfaces.senders.Sender;

import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Set;

/**
 * Envelopes service API.
 *
 * The purpose of this class is to handle all the operations related to the Envelope instances management.
 */
@Path("/api/envelopes")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EnvelopesService {

	EnvelopeDAO envelopeDAO;

	@Inject
	EnvelopesService(EnvelopeDAO envelopeDAO) {
		this.envelopeDAO = envelopeDAO;
	}

	@GET @Path("/all")
	public List<Envelope> getAll() {
		return envelopeDAO.findAll();
	}

	@GET @Path("{id}")
	public Envelope get(@PathParam("id") Long id) {
		return envelopeDAO.find(id);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Envelope create(Envelope envelope) {
		envelope = envelopeDAO.insert(envelope);
		return envelope;
	}

	@PUT @Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Envelope update(Envelope envelope) {
		envelope = envelopeDAO.update(envelope);
		return envelope;
	}

	@DELETE @Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void delete(@PathParam("id") Long id) {
		Envelope envelope = envelopeDAO.find(id);
		if (envelope != null) {
			envelopeDAO.delete(envelope);
		}
	}

	@GET @Path("/all/senders")
	public Set<Class<Sender>> getAvailableSenders() {
		return envelopeDAO.getAvailableSenderClasses();
	}
}
