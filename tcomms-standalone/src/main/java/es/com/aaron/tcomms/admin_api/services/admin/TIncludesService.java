package es.com.aaron.tcomms.admin_api.services.admin;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.models.TInclude;
import es.com.aaron.tcomms.core.models.daos.TIncludeDAO;

import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * TIncludes service API.
 *
 * The purpose of this class is to handle all the operations related to the TInclude instances management.
 */
@Path("/api/tincludes")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TIncludesService {

	TIncludeDAO tIncludeDAO;

	@Inject
	TIncludesService(TIncludeDAO tIncludeDAO) {
		this.tIncludeDAO = tIncludeDAO;
	}

	@GET @Path("/all")
	public List<TInclude> getAll() {
		return tIncludeDAO.findAll();
	}

	@GET @Path("{id}")
	public TInclude get(@PathParam("id") Long id) {
		return tIncludeDAO.find(id);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TInclude create(TInclude tInclude) {
		tInclude = tIncludeDAO.insert(tInclude);
		return tInclude;
	}

	@PUT @Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TInclude update(TInclude tInclude) {
		tInclude = tIncludeDAO.update(tInclude);
		return tInclude;
	}

	@DELETE @Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void delete(@PathParam("id") Long id) {
		TInclude tInclude = tIncludeDAO.find(id);
		if (tInclude != null) {
			tIncludeDAO.delete(tInclude);
		}
	}


}
