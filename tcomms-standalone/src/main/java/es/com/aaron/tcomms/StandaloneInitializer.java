package es.com.aaron.tcomms;

import com.google.inject.Injector;
import com.google.inject.servlet.GuiceFilter;
import es.com.aaron.tcomms.admin_api.EmptyServlet;
import es.com.aaron.tcomms.admin_api.ServletContextListener;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.platform.bootstrap.MainBootstrap;
import es.com.aaron.tcomms.platform.config.MainProperties;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

/**
 * Serves as main class to initialize the Administration Api WS.
 */
public class StandaloneInitializer {

	public static void main(String[] args) throws Exception {
		// Get injector and retrieve MainProperties instance
		Injector injector = MainBootstrap.getOrInitiateInjector();
		MainProperties mainProperties = injector.getInstance(MainProperties.class);
		// TODO Check overriding tcomms.properties
		mainProperties.overrideWithFile("/tcomms-standalone.properties");

		Server server = new Server(Integer.parseInt(mainProperties.getProperty("tcomms.servlet.port")));
		ServletContextListener contextListener = new ServletContextListener();

		ServletContextHandler contextHandler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);
		contextHandler.addEventListener(contextListener);
		contextHandler.addFilter(GuiceFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));
		contextHandler.addServlet(EmptyServlet.class, "/*"); // Needed empty servlet to get filters executed

		// Hack to avoid error when server.start() is called with empty resources.
		// TODO Figure out better solution for this
		if (Boolean.valueOf(mainProperties.getProperty("tcomms.servlet.api.enabled"))
				|| Boolean.valueOf(mainProperties.getProperty("tcomms.servlet.listener.enabled"))) {
			server.start();
			server.join();
		} else {
			Logger logger = injector.getInstance(LoggerProvider.class).getLogger(StandaloneInitializer.class);
			logger.info("Neither the API or the main listener are enabled. Process ended.");
		}

	}
}
