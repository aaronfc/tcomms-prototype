package es.com.aaron.tcomms.admin_api.services.admin;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.events.Event;
import es.com.aaron.tcomms.core.models.Communication;
import es.com.aaron.tcomms.core.models.Condition;
import es.com.aaron.tcomms.core.models.daos.CommunicationDAO;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Communications service API.
 *
 * The purpose of this class is to handle all the operations related to the Communication instances management.
 */
@Path("/api/communications")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommunicationsService {
	CommunicationDAO communicationDao;

	@Inject
	CommunicationsService(CommunicationDAO communicationDao) {
		this.communicationDao = communicationDao;
	}

	@GET @Path("{id}")
	public Communication get(@PathParam("id") Long id) {
		return communicationDao.find(id);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Communication create(Communication communication) {
		communication = communicationDao.insert(communication);
		return communication;
	}

	@PUT @Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Communication update(Communication communication) {
		communication = communicationDao.update(communication);
		return communication;
	}

	@DELETE @Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void delete(@PathParam("id") Long id) {
		Communication communication = communicationDao.find(id);
		if (communication != null) {
			communicationDao.delete(communication);
		}
	}

	@GET @Path("event/{eventName}/")
	public List<Communication> getCommunicationsByEvent(@PathParam("eventName") String eventName) {
		Event event = new Event(eventName, new ParamsBag());
		return communicationDao.getAllByEvent(event);
	}

	@POST @Path("addCondition/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// TODO Verify this behaviour. Maybe conditionId hast to be received instead of Condition
	public Communication addCondition(@PathParam("id") Long id, Condition condition) {
		Communication communication = communicationDao.find(id);
		if (communication != null) {
			communication.addCondition(condition);
		}
		return communication;
	}
}
