package es.com.aaron.tcomms.admin_api.di;

import com.google.inject.Inject;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.platform.config.MainProperties;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * ServletsModule class.
 *
 * Removed @DIModule from here, as this should be loaded by a child injector.
 */
public class ServletsModule extends com.google.inject.servlet.ServletModule {

	private final Logger logger;
	private MainProperties mainProperties;

	@Inject
	public ServletsModule(MainProperties mainProperties, LoggerProvider loggerProvider) {
		this.mainProperties = mainProperties;
		this.logger = loggerProvider.getLogger(getClass());
	}


    @Override
	protected void configureServlets() {
		/*
		* Explicitly bind GuiceContainer so that the child, not root, injector is injected into its constructor.
		* http://stackoverflow.com/questions/8454647/jersey-guice-doesnt-process-bound-resources-if-injector-is-a-child
		*/
	    bind(GuiceContainer.class);
	    if (Boolean.valueOf(mainProperties.getProperty("tcomms.servlet.api.enabled"))) {
		    load(new PackagesResourceConfig("es.com.aaron.tcomms.admin_api.services.admin"));
		    logger.info("Loaded resources for ADMIN API.");
	    }
	    if (Boolean.valueOf(mainProperties.getProperty("tcomms.servlet.listener.enabled"))) {
	        load(new PackagesResourceConfig("es.com.aaron.tcomms.admin_api.services.main"));
		    logger.info("Loaded resources for main listener.");
	    }

	    serve("/*").with(GuiceContainer.class);
	}

	/**
	 * Load resources.
	 * @param rc Resources config
	 */
    private void load(ResourceConfig rc) {
	    // Set configuration
        final Map<String, Object> config = new HashMap<String, Object>();
        config.put("com.sun.jersey.api.json.POJOMappingFeature", true);
        rc.setPropertiesAndFeatures(config);

        for ( Class<?> resource : rc.getClasses() ) {
	        logger.info("Binding resource: {}", resource.getName());
            bind(resource);
        }
    }
}
