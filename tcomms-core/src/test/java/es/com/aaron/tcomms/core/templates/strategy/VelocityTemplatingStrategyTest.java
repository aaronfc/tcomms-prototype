package es.com.aaron.tcomms.core.templates.strategy;

import com.google.inject.Injector;
import es.com.aaron.tcomms.core.models.TInclude;
import es.com.aaron.tcomms.core.models.daos.TIncludeDAO;
import es.com.aaron.tcomms.core.templates.strategy.velocity.VelocityTemplatingStrategy;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import org.junit.Test;
import org.mockito.Mock;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * VelocityTemplatingStrategy test.
 */
public class VelocityTemplatingStrategyTest {

	private VelocityTemplatingStrategy strategy;

	@Mock
	Injector injectorMock;

	@Mock
	TIncludeDAO tIncludeDAO;

	public VelocityTemplatingStrategyTest() {
		initMocks(this);
		strategy = new VelocityTemplatingStrategy(injectorMock, tIncludeDAO);
	}

	@Test
	public void testRenderWithoutParameters() throws Exception {
		ParamsBag parameters = new ParamsBag();
		String template = "This is a template without any required parameters.";

		String output = strategy.render(template, parameters);

		assertEquals(template, output);
	}

	@Test
	public void testRenderWithParameters() throws Exception {
		ParamsBag parameters = new ParamsBag();
		parameters.put("rootLevel", "value1");
		parameters.put("rootLevel2", "value2");
		ParamsBag fakeProvider = new ParamsBag();
		fakeProvider.put("subParam1", "value3");
		ParamsBag subParam2 = new ParamsBag();
		subParam2.put("subSubParam1", "value4");
		fakeProvider.put("subParam2", subParam2);
		parameters.put("fakeProvider", fakeProvider);
		String template = "This is a template with some parameters. " +
				"Direct parameters like $rootLevel, or $rootLevel2. " +
				"Others with more depth like $fakeProvider.subParam1 or $fakeProvider.subParam2.subSubParam1";

		String output = strategy.render(template, parameters);

		String expected = "This is a template with some parameters. " +
				"Direct parameters like value1, or value2. " +
				"Others with more depth like value3 or value4";

		assertEquals(expected, output);
	}

	@Test
	public void testParse() throws Exception {

		String template = "This is a test temaplate with some required parameters. " +
				"Like this one $basicParameter1 or this other $basicParameter2. " +
				"Text doesn't really matter but the parameters and the hierarchy they generate. " +
				"So parameters with more depth like $fakeProvider.value1 or $fakeProvider2.value1 are required. " +
				"Also verify how this would work with a more deeper parameters hierarchy like: " +
				"$fakeProvider3.fakeValueOfValues.value. " +
				"Also complicated structures are required.\n" +
				"#if( $conditionalParameterCondition )\n" +
				"   <strong>$conditionalParameterTrue</strong>\n" +
				"#else\n" +
				"   <strong>$conditionalParameterFalse</strong>\n" +
				"#end\n";

		Set<String> detectedKeysSet = strategy.parse(template);

		Set<String> expected = new HashSet<String>();
		expected.add("basicParameter1");
		expected.add("basicParameter2");
		expected.add("fakeProvider.value1");
		expected.add("fakeProvider2.value1");
		expected.add("fakeProvider3.fakeValueOfValues.value");
		expected.add("conditionalParameterCondition");
		expected.add("conditionalParameterTrue");
		expected.add("conditionalParameterFalse");

		assertEquals(expected, detectedKeysSet);

	}


	@Test
	public void testParseWithTInclude() throws Exception {
		when(injectorMock.getInstance(TIncludeDAO.class)).thenReturn(tIncludeDAO);
		when(injectorMock.getInstance(TemplatingStrategy.class)).thenReturn(strategy);
		Map<String, TInclude> availableMap = new HashMap<String, TInclude>();
		availableMap.put("myInclude", new TInclude("My include", "This is $included. $fakeIncludedProvider.value1"));
		when(tIncludeDAO.getAllAsMap()).thenReturn(availableMap);

		String template = "#tinclude(\"myInclude\")\n" +
				"$basicParameter1, $basicParameter2" +
				" $fakeProvider.value1";

		Set<String> detectedKeysSet = strategy.parse(template);

		Set<String> expected = new HashSet<String>();
		expected.add("basicParameter1");
		expected.add("basicParameter2");
		expected.add("fakeProvider.value1");
		expected.add("included");
		expected.add("fakeIncludedProvider.value1");

		assertEquals(expected, detectedKeysSet);

	}

	@Test
	public void testRenderWithTInclude() throws Exception {
		when(injectorMock.getInstance(TIncludeDAO.class)).thenReturn(tIncludeDAO);
		when(injectorMock.getInstance(TemplatingStrategy.class)).thenReturn(strategy);
		Map<String, TInclude> availableMap = new HashMap<String, TInclude>();
		availableMap.put("myInclude", new TInclude("My include", "This is $included. $fakeIncludedProvider.value1\n"));
		when(tIncludeDAO.getAllAsMap()).thenReturn(availableMap);

		String template = "#tinclude(\"myInclude\")\n" +
				"$basicParameter1, $basicParameter2" +
				" $fakeProvider.value1";

		ParamsBag parameters = new ParamsBag();
		parameters.put("basicParameter1", "value1");
		parameters.put("basicParameter2", "value2");
		parameters.put("fakeProvider.value1", "value3");
		parameters.put("included", "value4");
		parameters.put("fakeIncludedProvider.value1", "value5");

		String output = strategy.render(template, parameters);

		String expected = "This is value4. value5\n" +
				"value1, value2 value3";

		assertEquals(expected, output);
	}
}
