package es.com.aaron.tcomms.core.provisioning.resolvers;

import es.com.aaron.tcomms.interfaces.data_providers.DataProvider;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * FirstMatchingParametersResolver test
 */
public class FirstMatchingParametersResolverTest {

	private ParametersResolverStrategy strategy;

	@Before
	public void setUp() throws Exception {
		strategy = new FirstMatchingParametersResolver();
	}


	//TODO @Test
	public void testResolveParametersSimpleCase() throws Exception {
		ParamsBag input = new ParamsBag();
		input.put("input1", "input1_value");

		Set<DataProvider> dataProviders = new HashSet<DataProvider>();
		dataProviders.add(mockDataProvider("DP1", Arrays.asList("input1"), Arrays.asList("param1", "param2")));
		dataProviders.add(mockDataProvider("DP2", Arrays.asList("input2"), Arrays.asList("param3", "param4")));

		Set<String> required = new HashSet<String>(Arrays.asList("DP1.param1"));

		ParamsBag output = strategy.resolveParameters(input, dataProviders, required);
		assertTrue(output.containsKey("DP1"));
		assertFalse(output.containsKey("DP2")); // input2 is required and not present
	}

	//TODO @Test
	public void testResolveParametersSimpleDependencyCase() throws Exception {
		ParamsBag input = new ParamsBag();
		input.put("input1", "input1_value");

		Set<DataProvider> dataProviders = new HashSet<DataProvider>();
		dataProviders.add(mockDataProvider("DP1", Arrays.asList("input1"), Arrays.asList("param1", "param2")));
		dataProviders.add(mockDataProvider("DP2", Arrays.asList("param2"), Arrays.asList("param3", "param4")));
		dataProviders.add(mockDataProvider("DP3", Arrays.asList("param4"), Arrays.asList("param5", "param6")));
		dataProviders.add(mockDataProvider("DP4", Arrays.asList("param7"), Arrays.asList("param8", "param9")));

		Set<String> required = new HashSet<String>(Arrays.asList("DP3.param6"));

		ParamsBag output = strategy.resolveParameters(input, dataProviders, required);
		assertTrue(output.containsKey("DP1"));
		assertTrue(output.containsKey("DP2"));
		assertTrue(output.containsKey("DP3"));
		assertFalse(output.containsKey("DP4")); // param 7 could not be generated
	}

	private DataProvider mockDataProvider(String prefix, List<String> inputParams, List<String> allParams) {
		DataProvider dataProviderMock = mock(DataProvider.class);
		when(dataProviderMock.getInputParameterKeys()).thenReturn(new HashSet<String>(inputParams));
		when(dataProviderMock.getAllParametersKeys()).thenReturn(new HashSet<String>(allParams));
		when(dataProviderMock.getPrefix()).thenReturn(prefix);
		return dataProviderMock;
	}
}
