package es.com.aaron.tcomms.core.conditions.strategy;

import es.com.aaron.tcomms.core.conditions.strategy.exceptions.UnavailableVariableException;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * MVELConditionEvaluatorStrategy test.
 */
@RunWith(value = Parameterized.class)
public class MVELConditionEvaluatorStrategyParameterizedTest {

	private String condition;

	private ParamsBag parameters;

	private Boolean expectedResult;

	private Boolean expectedException;

	private ConditionEvaluatorStrategy evaluator = new MVELConditionEvaluatorStrategy();

	public MVELConditionEvaluatorStrategyParameterizedTest(String condition, ParamsBag parameters, Boolean expectedResult,
	                                                       Boolean expectedException) {
		this.condition = condition;
		this.parameters = parameters;
		this.expectedResult = expectedResult;
		this.expectedException = expectedException;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> data() {
		ParamsBag paramsSet = new ParamsBag();
		paramsSet.put("a", "value1");
		paramsSet.put("b", "value1");
		paramsSet.put("c", "value1");
		paramsSet.put("d", "value2");
		paramsSet.put("e", 1);
		paramsSet.put("f", 2);
		return Arrays.asList(new Object[][] {
				{"a=='value1'", paramsSet, Boolean.TRUE, Boolean.FALSE},
				{"a=='value2'", paramsSet, Boolean.FALSE, Boolean.FALSE},
				{"a==b",new ParamsBag(), Boolean.FALSE, Boolean.TRUE},
				{"a==b", paramsSet, Boolean.TRUE, Boolean.FALSE},
				{"b==c", paramsSet, Boolean.TRUE, Boolean.FALSE},
				{"c==d", paramsSet, Boolean.FALSE, Boolean.FALSE},
				{"c!=d", paramsSet, Boolean.TRUE, Boolean.FALSE},
				{"e<f", paramsSet, Boolean.TRUE, Boolean.FALSE},
				{"f<e", paramsSet, Boolean.FALSE, Boolean.FALSE},
				{"e+f==3", paramsSet, Boolean.TRUE, Boolean.FALSE},
				{"e+f==2", paramsSet, Boolean.FALSE, Boolean.FALSE}
		});
	}

	@Test
	public void testCheck() throws Exception {
		try {
			Boolean result = evaluator.check(condition, parameters);
			if(expectedException) {
				fail();
			} else {
				assertEquals(expectedResult, result);
			}
		} catch(UnavailableVariableException ignored) {}
	}
}
