package es.com.aaron.tcomms.core.provisioning.extractors;

import es.com.aaron.tcomms.core.templates.strategy.TemplatingStrategy;
import es.com.aaron.tcomms.interfaces.senders.SenderFields;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * SenderFieldsParametersExtractor unit test
 */
public class SenderFieldsParametersExtractorUnitTest {

	@Mock
	TemplatingStrategy strategy;

	ParamsExtractor extractor;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		extractor = new SenderFieldsParamsExtractor(strategy);
	}

	@Test
	public void testExtractFromSenderFields() throws Exception {
		SenderFields senderFields = new SenderFields();
		senderFields.put("field1", "value1");
		senderFields.put("field2", "value2");

		Set<String> value1Params = new HashSet<String>();
		value1Params.add("one");
		value1Params.add("two.three");
		when(strategy.parse("value1")).thenReturn(value1Params);

		Set<String> value2Params = new HashSet<String>();
		value2Params.add("caramba");
		when(strategy.parse("value2")).thenReturn(value2Params);

		Set<String> output = extractor.extract(senderFields);

		Set<String> expected = new HashSet<String>();
		expected.add("one");
		expected.add("two.three");
		expected.add("caramba");

		assertEquals(expected, output);
	}
}
