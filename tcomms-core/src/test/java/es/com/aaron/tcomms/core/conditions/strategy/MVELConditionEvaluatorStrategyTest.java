package es.com.aaron.tcomms.core.conditions.strategy;

import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Conditions evaluator strategy using MVEL.
 */
public class MVELConditionEvaluatorStrategyTest {

	private ConditionEvaluatorStrategy evaluator = new MVELConditionEvaluatorStrategy();

	@Test
	public void testBasicParse() throws Exception {
		Set<String> output = evaluator.parse("a==b");

		Set<String> expected = new HashSet<String>();
		expected.add("a");
		expected.add("b");

		assertEquals(expected, output);
	}

	@Test
	public void testParseWithNestedParams() throws Exception {
		Set<String> output = evaluator.parse("a.b==b.c && d");

		Set<String> expected = new HashSet<String>();
		expected.add("a.b");
		expected.add("b.c");
		expected.add("d");

		assertEquals(expected, output);
	}

	@Test
	public void testParseWithNestedParamsExtra() throws Exception {
		Set<String> output = evaluator.parse("a.b==b.c.d.e.f && d || ca.ca");

		Set<String> expected = new HashSet<String>();
		expected.add("a.b");
		expected.add("b.c.d.e.f");
		expected.add("d");
		expected.add("ca.ca");

		assertEquals(expected, output);
	}

	@Test
	public void testCheckWithParamsWithDots() throws Exception {
		ParamsBag params = new ParamsBag();
		params.put("a.b.c", "a");
		assertTrue(evaluator.check("a.b.c=='a'", params));
	}

}
