package es.com.aaron.tcomms.core.provisioning.extractors;

import es.com.aaron.tcomms.core.conditions.strategy.ConditionEvaluatorStrategy;
import es.com.aaron.tcomms.core.models.Condition;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * ConditionParametersExtractorImpl test
 */
public class ConditionParametersExtractorUnitTest {

	@Mock
	ConditionEvaluatorStrategy strategy;

	ParamsExtractor extractor;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		extractor = new ConditionParamsExtractor(strategy);
	}

	@Test
	public void testExtractFromCondition() throws Exception {
		Condition condition = new Condition("Test1", "condition");

		Set<String> expected = new HashSet<String>();
		expected.add("this.is.test");
		when(strategy.parse("condition")).thenReturn(expected);

		Set<String> output = extractor.extract(condition);

		assertEquals(expected, output);
	}

	@Test
	public void testExtractFromMultipleConditions() throws Exception {
		Set<Condition> conditionSet = new HashSet<Condition>();
		conditionSet.add(new Condition("Test1", "condition1"));
		conditionSet.add(new Condition("Test1", "condition2"));

		Set<String> expected1 = new HashSet<String>();
		expected1.add("this.is.test1");
		when(strategy.parse("condition1")).thenReturn(expected1);
		Set<String> expected2 = new HashSet<String>();
		expected2.add("this.is.test2");
		when(strategy.parse("condition2")).thenReturn(expected2);

		Set<String> output = extractor.extract(conditionSet.toArray());
		Set<String> expected = new HashSet<String>();
		expected.add("this.is.test1");
		expected.add("this.is.test2");

		assertEquals(expected, output);
	}
}
