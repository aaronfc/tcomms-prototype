package es.com.aaron.tcomms.core.events;

import es.com.aaron.tcomms.core.conditions.ConditionChecker;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.core.messages.Message;
import es.com.aaron.tcomms.core.messages.MessageFactory;
import es.com.aaron.tcomms.core.messages.MessageProcessor;
import es.com.aaron.tcomms.core.models.Communication;
import es.com.aaron.tcomms.core.models.Condition;
import es.com.aaron.tcomms.core.models.Envelope;
import es.com.aaron.tcomms.core.models.daos.CommunicationDAO;
import es.com.aaron.tcomms.core.notifications.NotificationsService;
import es.com.aaron.tcomms.core.provisioning.DataProvisionService;
import es.com.aaron.tcomms.core.provisioning.extractors.ConditionParamsExtractor;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import es.com.aaron.tcomms.interfaces.senders.SenderFields;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * EventProcessorImpl test
 */
public class EventProcessorImplTest {

	@Mock
	private DataProvisionService provisionService;
	@Mock
	private CommunicationDAO communicationDao;
	@Mock
	private ConditionChecker conditionChecker;
	@Mock
	private MessageProcessor messageProcessor;
	@Mock
	private ConditionParamsExtractor conditionParamsExtractor;
	@Mock
	private MessageFactory messageFactory;
	@Mock
	private NotificationsService notificationsService;
	@Mock
	private Event event;
	@Mock
	private LoggerProvider loggerProviderMock;

	@Before
	public void setUp() throws Exception {
		initMocks(this);
	}

	// TODO @Test
	public void testOnEventRetrievesCommAndProcessesIt() throws Exception {
		// TODO  Redo this test, updated to allow compilation.
		// Prepare input
		ParamsBag inputParams = new ParamsBag();
		inputParams.put("param1", "value1");
		Event inputEvent = new Event("MY_EVENT", inputParams);

		// Prepare conditions and communications DAO
		Set<Condition> conditionsMockSet = new HashSet<Condition>(Arrays.asList(mock(Condition.class)));
		List<Communication> communicationsMockList = Arrays.asList(mock(Communication.class));
		when(communicationsMockList.get(0).getConditions()).thenReturn(conditionsMockSet);
		when(communicationDao.getAllByEvent(inputEvent)).thenReturn(communicationsMockList);

		// Prepare parameters for conditions check
		Set<String> conditionSetParamsKeys = new HashSet<String>();
		ParamsBag conditionParams = new ParamsBag();
		when(conditionParamsExtractor.extractFromAllCommunications(communicationsMockList)).thenReturn(conditionSetParamsKeys);
		when(provisionService.getParameters(inputParams, conditionSetParamsKeys)).thenReturn(conditionParams);
		when(conditionChecker.evaluate(conditionsMockSet, conditionParams)).thenReturn(true);

		// Prepare parameters for processing check
		SenderFields senderFieldsMock = mock(SenderFields.class);
		Envelope envelopeMock = mock(Envelope.class);
		Set<String> senderFieldsParamsKeys = new HashSet<String>();
		ParamsBag senderFieldsParams = new ParamsBag();
		when(communicationsMockList.get(0).getEnvelope()).thenReturn(envelopeMock);
		when(envelopeMock.getSenderData()).thenReturn(senderFieldsMock);
		when(provisionService.getParameters(inputParams, senderFieldsParamsKeys)).thenReturn(senderFieldsParams);
		Message message = mock(Message.class);
		when(messageFactory.create(any(Event.class), any(Communication.class))).thenReturn(message);

		EventProcessor eventProcessor = new EventProcessorImpl(provisionService, communicationDao, conditionChecker,
				messageProcessor, conditionParamsExtractor, notificationsService, messageFactory, loggerProviderMock, null, null, null);
		eventProcessor.onEvent(inputEvent);

		verify(messageProcessor, times(1)).process(message);
	}
}
