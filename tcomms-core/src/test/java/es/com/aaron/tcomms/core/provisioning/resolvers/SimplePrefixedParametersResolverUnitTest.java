package es.com.aaron.tcomms.core.provisioning.resolvers;

import es.com.aaron.tcomms.interfaces.data_providers.DataProvider;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anySet;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * SimplePrefixedParametersResolver unit test.
 */
public class SimplePrefixedParametersResolverUnitTest {

	private ParametersResolverStrategy strategy;

	@Before
	public void setUp() throws Exception {
		strategy = new SimplePrefixedParametersResolver();
	}

	@Test
	public void testResolveParametersSimpleIndependentDataProviders() throws Exception {
		Set<DataProvider> dataProviders = new HashSet<DataProvider>();
		dataProviders.add(mockDataProvider("DP1", Arrays.asList("input1"), Arrays.asList("param1", "param2")));
		dataProviders.add(mockDataProvider("DP2", Arrays.asList("input2"), Arrays.asList("param3", "param4")));

		Set<String> required = new HashSet<String>(Arrays.asList("DP1.param1", "DP2.param3"));
		ParamsBag input = new ParamsBag();
		input.put("input1", "value1");
		input.put("input2", "value2");

		ParamsBag output = strategy.resolveParameters(input, dataProviders, required);
		assertTrue(output.containsKey("DP1.param1"));
		assertTrue(output.containsKey("DP2.param3"));
	}

	@Test
	public void testResolveParametersIgnoredNonRequiredDataProvider() throws Exception {
		Set<DataProvider> dataProviders = new HashSet<DataProvider>();
		dataProviders.add(mockDataProvider("DP1", Arrays.asList("input1"), Arrays.asList("param1", "param2")));
		dataProviders.add(mockDataProvider("DP2", Arrays.asList("input2"), Arrays.asList("param3", "param4")));

		Set<String> required = new HashSet<String>(Arrays.asList("DP2.param3"));
		ParamsBag input = new ParamsBag();
		input.put("input1", "value1");
		input.put("input2", "value2");

		ParamsBag output = strategy.resolveParameters(input, dataProviders, required);
		assertFalse(output.containsKey("DP1"));
		assertTrue(output.containsKey("DP2.param3"));
	}

	@Test
	public void testResolveParametersParametersWithoutPrefixAreNotFound() throws Exception {
		Set<DataProvider> dataProviders = new HashSet<DataProvider>();
		dataProviders.add(mockDataProvider("DP1", Arrays.asList("input1"), Arrays.asList("param1", "param2")));
		dataProviders.add(mockDataProvider("DP2", Arrays.asList("input2"), Arrays.asList("param3", "param4")));

		Set<String> required = new HashSet<String>(Arrays.asList("param3"));
		ParamsBag input = new ParamsBag();
		input.put("input1", "value1");
		input.put("input2", "value2");

		ParamsBag output = strategy.resolveParameters(input, dataProviders, required);
		assertFalse(output.containsKey("DP1"));
		assertFalse(output.containsKey("DP2"));
	}

	private DataProvider mockDataProvider(String prefix, List<String> inputParams, List<String> allParams) {
		DataProvider dataProviderMock = mock(DataProvider.class);
		when(dataProviderMock.getInputParameterKeys()).thenReturn(new HashSet<String>(inputParams));
		when(dataProviderMock.getAllParametersKeys()).thenReturn(new HashSet<String>(allParams));
		ParamsBag params = new ParamsBag();
		for(String key : allParams) {
			params.put(key, "FAKE_VALUE");
		}
		when(dataProviderMock.getRemoteParameters(any(ParamsBag.class), anySet())).thenReturn(params);
		when(dataProviderMock.getPrefix()).thenReturn(prefix);
		return dataProviderMock;
	}
}
