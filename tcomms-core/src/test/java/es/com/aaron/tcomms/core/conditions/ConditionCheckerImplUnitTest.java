package es.com.aaron.tcomms.core.conditions;

import es.com.aaron.tcomms.core.conditions.strategy.ConditionEvaluatorStrategy;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.core.models.Condition;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * ConditionCheckerImpl unit test.
 */
public class ConditionCheckerImplUnitTest {

	@Mock
	private ConditionEvaluatorStrategy evaluatorMock;

	@Mock
	private LoggerProvider loggerProviderMock;

	private ConditionChecker checker;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		checker = new ConditionCheckerImpl(evaluatorMock, loggerProviderMock);
	}

	@Test
	public void testAllowed() throws Exception {
		ParamsBag params = new ParamsBag();
		Condition condition1 = mock(Condition.class);
		Condition condition2 = mock(Condition.class);
		Set<Condition> conditionMocks = new HashSet<Condition>(Arrays.asList(condition1, condition2));
		when(condition1.getConditionString()).thenReturn("condition1");
		when(condition2.getConditionString()).thenReturn("condition2");
		when(evaluatorMock.check("condition1", params)).thenReturn(true);
		when(evaluatorMock.check("condition2", params)).thenReturn(true);

		assertTrue(checker.evaluate(conditionMocks, params));
		verify(evaluatorMock, times(1)).check("condition1", params);
		verify(evaluatorMock, times(1)).check("condition2", params);
	}
}
