package es.com.aaron.tcomms.core.utils;

import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * ParameterUtils test file
 */
public class ParameterUtilsUnitTest {
	@Test
	public void testIsPrefixed() throws Exception {
		assertEquals(true, ParameterUtils.isPrefixed("prefixed.key"));
		assertEquals(true, ParameterUtils.isPrefixed("prefixed.key.with.dots"));
		assertEquals(false, ParameterUtils.isPrefixed("unprefixedkey"));
	}

	@Test
	public void testGetPrefix() throws Exception {
		assertEquals("prefixed", ParameterUtils.getPrefix("prefixed.key"));
		assertEquals("prefixed", ParameterUtils.getPrefix("prefixed.key.with.dots"));
		assertEquals("", ParameterUtils.getPrefix("unprefixedkey"));
	}

	@Test
	public void testGetSimpleName() throws Exception {
		assertEquals("key", ParameterUtils.getSimpleName("prefixed.key"));
		assertEquals("key.with.dots", ParameterUtils.getSimpleName("prefixed.key.with.dots"));
		assertEquals("unprefixedkey", ParameterUtils.getSimpleName("unprefixedkey"));
	}

	@Test
	public void testGetPrefixedParamsWithoutPrefix() throws Exception {
		ParamsBag bag = new ParamsBag();
		bag.put("PREFIX.key", "VALUE1");
		bag.put("PREFIX.subkey.key", "VALUE2");
		bag.put("OTHERPREF.key", "VALUE3");
		bag.put("key", "VALUE4");
		ParamsBag out = ParameterUtils.getPrefixedParams(bag, "PREFIX");
		assertEquals("VALUE1", out.get("key"));
		assertEquals("VALUE2", out.get("subkey.key"));
		assertEquals(2, out.entrySet().size());
	}

	@Test
	public void testGenerateKey() throws Exception {
		assertEquals("a", ParameterUtils.generateKey("a"));
		assertEquals("", ParameterUtils.generateKey(""));
		assertEquals("", ParameterUtils.generateKey());
		assertEquals("a.b.c", ParameterUtils.generateKey("a", "b", "c"));
		assertEquals("a..b.c", ParameterUtils.generateKey("a.", "b", "c"));
	}
}
