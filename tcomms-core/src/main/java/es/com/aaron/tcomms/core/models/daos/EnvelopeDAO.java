package es.com.aaron.tcomms.core.models.daos;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.core.models.Envelope;
import es.com.aaron.tcomms.interfaces.senders.Sender;
import es.com.aaron.tcomms.platform.config.MainProperties;
import es.com.aaron.tcomms.platform.persistence.GenericDAOImpl;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**
 * EnvelopeDAO class.
 */
public class EnvelopeDAO extends GenericDAOImpl<Envelope, Long> {

	private final Logger logger;
	private MainProperties mainProperties;

	@Inject
	public EnvelopeDAO(MainProperties mainProperties, LoggerProvider loggerProvider) {
		this.mainProperties = mainProperties;
		this.logger = loggerProvider.getLogger(getClass());
	}

	/**
	 * Method to get available sender classes.
	 *
	 * TODO Move this where required. It is here because it is used only from the EnvelopeService from admin API.
	 * @return Set of all classes implementing Sender interface
	 */
	public Set<Class<Sender>> getAvailableSenderClasses() {
		Set<Class<Sender>> set = new HashSet<Class<Sender>>();

		// TODO This logic might end up encapsulated in a classes crawler class.
		Boolean autodiscovery = Boolean.valueOf(mainProperties.getProperty("tcomms.senders.autodiscovery"));
		if (autodiscovery) {
			logger.error("Auto-discovery for Senders is not implemented yet.");
		} else {
			// We use here Guice. External dataproviders should use Guice for injection or allow an empty constructor.
			String providersString = mainProperties.getProperty("tcomms.senders", "");
			String[] classes = providersString.split(",");
			for(String clazzString : classes) {
				try {
					Class clazz = Class.forName(clazzString.trim());
					set.add((Class<Sender>) clazz);
				} catch (ClassNotFoundException e) {
					logger.error(String.format("Could not find sender \"%s\".", clazzString), e);
				}
			}
		}

		return set;
	}
}
