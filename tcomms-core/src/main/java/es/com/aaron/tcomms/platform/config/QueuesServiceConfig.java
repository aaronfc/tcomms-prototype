package es.com.aaron.tcomms.platform.config;

/**
 * QueuesServiceConfig interface.
 */
public interface QueuesServiceConfig {
	/**
	 * Get host where queues are hosted.
	 * @return Host
	 */
    public String getHost();

	/**
	 * Get queue name for event processing.
	 * @return Event queue name
	 */
    public String getEventQueueName();

	/**
	 * Get queue name for communications processing.
	 * @return Communications queue name
	 */
    public String getProcessesQueueName();
}
