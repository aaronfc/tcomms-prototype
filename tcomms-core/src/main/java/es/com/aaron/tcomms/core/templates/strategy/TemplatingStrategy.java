package es.com.aaron.tcomms.core.templates.strategy;

import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import java.util.Set;

/**
 * TemplatingStrategy interface.
 */
public interface TemplatingStrategy {
	/**
	 * Method in charge of rendering correctly the given input with a set of data.
	 * @param input Input string with patterns to be rendered with the given data.
	 * @param data Data to make the rendering possible.
	 * @return Parsed string with all the replacements done.
	 */
	public String render(String input, ParamsBag data);

	/**
	 * Retrieve all required parameters for rendering the given template.
	 * @param input Input string given as template.
	 * @return Data structure with the parameters hierarchy required for rendering the given template.
	 */
	public Set<String> parse(String input);
}
