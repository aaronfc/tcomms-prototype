package es.com.aaron.tcomms.core.notifications;

import es.com.aaron.tcomms.core.events.Event;
import es.com.aaron.tcomms.core.messages.Message;

/**
 * Actions observer service.
 *
 * This interface defines how the actions are observed during the full process.
 */
public interface NotificationsService {
	/**
	 * An action occurred during the processing of an Event.
	 * @param event
	 * @param action
	 */
	public void notifyEventAction(Event event, EventAction action);

	/**
	 * An exception occurred while processing an Event.
	 * @param event
	 * @param e
	 */
	public void notifyEventException(Event event, Exception e);

	/**
	 * An action occurred during the processing of a Message.
	 * @param message
	 * @param action
	 */
	public void notifyMessageAction(Message message, MessageAction action);

	/**
	 * An exception occurred while processing a Message.
	 * @param message
	 * @param e
	 */
	public void notifyMessageException(Message message, Exception e);
}
