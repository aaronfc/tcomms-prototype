package es.com.aaron.tcomms.core.templates.strategy.velocity;

import com.google.inject.Inject;
import com.google.inject.Injector;
import es.com.aaron.tcomms.core.models.TInclude;
import es.com.aaron.tcomms.core.models.daos.TIncludeDAO;
import es.com.aaron.tcomms.core.templates.strategy.TemplatingStrategy;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeInstance;
import org.apache.velocity.runtime.parser.ParseException;
import org.apache.velocity.runtime.parser.node.ASTDirective;
import org.apache.velocity.runtime.parser.node.ASTReference;
import org.apache.velocity.runtime.parser.node.Node;
import org.apache.velocity.runtime.parser.node.SimpleNode;

import java.io.StringWriter;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * VelocityTemplatingStrategy class.
 *
 * This implements a TemplatingStrategy to be used by the TemplateEngine. Is in charge of defining
 * how the parsing is done. This implementation uses Apache Velocity.
 * @see <a href="http://velocity.apache.org/">Apache Velocity</a>
 */
public class VelocityTemplatingStrategy implements TemplatingStrategy {

	VelocityEngine engine;
	RuntimeInstance runtimeInstance;
	TIncludeDAO tIncludeDAO;

	@Inject
	public VelocityTemplatingStrategy(Injector injector, TIncludeDAO tIncludeDAO) {
		engine = new VelocityEngine();
		runtimeInstance = new RuntimeInstance();
		// Required application attribute for TIncludeDirective
		engine.setApplicationAttribute("di_injector", injector);
		engine.loadDirective(TIncludeDirective.class.getName());
		this.tIncludeDAO = tIncludeDAO;
	}

	public String render(String input, ParamsBag data) {
		VelocityContext context = new VelocityContext(data);
		StringWriter writer = new StringWriter();
		engine.evaluate(context, writer, "VelocityTemplating", input);
		return writer.toString();
	}

	@Override
	public Set<String> parse(String input) {
		Set<String> output;
		Set<String> references;

		try {
			SimpleNode node = runtimeInstance.parse(input, "");
			references = getReferences(node);
		} catch (ParseException e) {
			// TODO Handle this
			references = new HashSet<String>();
			e.printStackTrace();
		}

		Set<String> cleanReferences = cleanReferences(references);
		output = new HashSet<String>(cleanReferences);

		return output;
	}

	private Set<String> getReferences(Node node) {
		Set<String> references = new HashSet<String>();
		for (int i=0; i < node.jjtGetNumChildren(); i++) {
			Node subNode = node.jjtGetChild(i);
			if (subNode instanceof ASTReference) {
				references.add(subNode.literal());
			} else if(subNode instanceof ASTDirective) {
				if (((ASTDirective) subNode).getDirectiveName().equals("tinclude")) {
					Map<String, TInclude> allTIncludes = tIncludeDAO.getAllAsMap();
					for(int j=0; j < subNode.jjtGetNumChildren(); j++) {
						Node directiveSubNode = subNode.jjtGetChild(j);
						String tIncludeName = directiveSubNode.literal().replace("\"", "").replace("'", "");
						if (allTIncludes.containsKey(tIncludeName)) {
							TInclude tInclude = allTIncludes.get(tIncludeName);
							Set<String> tIncludeParams = this.parse(tInclude.getContent());
							references.addAll(tIncludeParams);
						}
					}
				}
			} else {
				if (subNode.jjtGetNumChildren() > 0) {
					Set<String> subReferences = getReferences(subNode);
					references.addAll(subReferences);
				}
			}
		}
		return references;
	}

	private Set<String> cleanReferences(Set<String> references) {
		Set<String> output = new HashSet<String>();
		for(String reference : references) {
			String cleanReference = reference.replace("$", "");
			output.add(cleanReference);
		}
		return output;
	}
}
