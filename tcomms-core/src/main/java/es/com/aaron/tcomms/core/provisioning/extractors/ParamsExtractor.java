package es.com.aaron.tcomms.core.provisioning.extractors;


import java.util.Set;

/**
 * Parameters extractor interface.
 */
public interface ParamsExtractor {
	public Set<String> extract(Object... inputObjects);
}
