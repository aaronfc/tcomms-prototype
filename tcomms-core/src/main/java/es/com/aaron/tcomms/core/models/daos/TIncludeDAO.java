package es.com.aaron.tcomms.core.models.daos;

import es.com.aaron.tcomms.core.models.TInclude;
import es.com.aaron.tcomms.platform.persistence.GenericDAOImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TIncludeDAO class.
 */
public class TIncludeDAO extends GenericDAOImpl<TInclude, Long> {
	/**
	 * Retrieve all TIncludes as a map where the key is the unique identifier and the value a TInclude instance.
	 * @return Map of current TIncludes
	 */
	public Map<String, TInclude> getAllAsMap() {
		Map<String, TInclude> map = new HashMap<String, TInclude>();
		List<TInclude> list = this.getQuery("GetAll").getResultList();
		for (TInclude include : list) {
			map.put(include.getTitle(), include);
		}

		return map;
	}

}
