package es.com.aaron.tcomms.core.provisioning.resolvers;

import es.com.aaron.tcomms.core.utils.ParameterUtils;
import es.com.aaron.tcomms.interfaces.data_providers.DataProvider;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import java.util.*;

/**
 * SimplePrefixedParametersResolver class.
 *
 * This parameters resolver strategy will resolve parameters that are correctly prefixed with its data provider prefix.
 * Only initial input parameters will be used as input parameters for each data provider.
 *
 * Check SimplePrefixedParametersResolverUnitTest to see more about this resolver.
 */
public class SimplePrefixedParametersResolver implements ParametersResolverStrategy {

	@Override
	public ParamsBag resolveParameters(ParamsBag inputParams, Set<DataProvider> providers, Set<String> required) {
		ParamsBag output = new ParamsBag();
		output.putAll(inputParams);

		for(DataProvider provider : providers) {
			Set<String> matchingKeys = ParameterUtils.getPrefixedParamKeys(required, provider.getPrefix());
			if (matchingKeys.size() > 0) {
				if (inputParams.containsKeySet(provider.getInputParameterKeys())) {
					output.put(provider.getPrefix(), provider.getRemoteParameters(inputParams, matchingKeys));
				}
			}
		}

		return output;
	}
}