package es.com.aaron.tcomms.core.conditions.strategy;

import es.com.aaron.tcomms.core.conditions.strategy.exceptions.UnavailableVariableException;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import org.mvel2.MVEL;
import org.mvel2.ParserContext;
import org.mvel2.PropertyAccessException;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Conditions evaluation strategy using MVEL.
 *
 * This is a implementation of a conditions evaluation strategy using MVEL.
 * @see <a href="http://mvel.codehaus.org/">MVEL</a>
 */
public class MVELConditionEvaluatorStrategy implements ConditionEvaluatorStrategy {

	@Override
	public Boolean check(String string, ParamsBag params) throws UnavailableVariableException {
		Boolean result;
		try {
			result = MVEL.eval(string, params, Boolean.class);
		} catch (PropertyAccessException e) {
			// TODO Verify if exact variable name can be retrieved
			throw new UnavailableVariableException(e.getErrors().toString());
		}
		return result;
	}

	@Override
	public Set<String> parse(String string) {
		Set<String> output = new HashSet<String>();

		ParserContext ctx = ParserContext.create();
		MVEL.analysisCompile(string, ctx);

		Map<String, Class> inputs = ctx.getInputs();

		// Complex parsing with regexp because there seems not to be an available form of looking for full references
		// This will get not only the initial key of an expected parameter but the full path of a expected parameter.
		// TODO Reimplement this trying to avoid hacky regexps
		for(Map.Entry<String, Class> inputEntry : inputs.entrySet()) {
			String inputLiteral = inputEntry.getKey();
			Pattern pattern = Pattern.compile("([^a-zA-Z0-9_\\.]|^)(" + inputLiteral + "\\.[a-zA-Z0-9_\\.]+)");
			Matcher matcher = pattern.matcher(string);
			boolean found = false;
			while(matcher.find()) {
				found = true;
				String piece = matcher.group(2);
				output.add(piece);
			}
			if (!found) {
				output.add(inputLiteral);
			}
		}

		return output;
	}
}
