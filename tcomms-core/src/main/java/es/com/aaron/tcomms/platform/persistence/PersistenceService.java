package es.com.aaron.tcomms.platform.persistence;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.PersistService;

import java.io.Closeable;

/**
 * PersistenceService class.
 */
@Singleton
public class PersistenceService implements Closeable {

	private PersistService service;

	@Inject
	public PersistenceService(final PersistService service) {
		this.service = service;
		this.service.start();
	}

	@Override
	public void close() {
		service.stop();
	}

}
