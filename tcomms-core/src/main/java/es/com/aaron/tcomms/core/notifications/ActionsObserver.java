package es.com.aaron.tcomms.core.notifications;

import es.com.aaron.tcomms.core.events.Event;
import es.com.aaron.tcomms.core.messages.Message;

/**
 * ActionsObserver interface.
 *
 * This has to be implemented in order to be subscribed to all the actions that occur during all the processing.
 */
public interface ActionsObserver {
	/**
	 * An action occurred during the processing of an Event.
	 * @param event
	 * @param action
	 */
	public void onEventAction(Event event, EventAction action);

	/**
	 * An exception occurred while processing an Event.
	 * @param event
	 * @param e
	 */
	void onEventException(Event event, Exception e);

	/**
	 * An action occurred during the processing of a Message.
	 * @param message
	 * @param action
	 */
	public void onMessageAction(Message message, MessageAction action);

	/**
	 * An exception occurred while processing a Message.
	 * @param message
	 * @param e
	 */
	void onMessageException(Message message, Exception e);
}
