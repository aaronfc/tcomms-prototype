package es.com.aaron.tcomms.platform.di_modules;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.persist.jpa.JpaPersistModule;
import es.com.aaron.tcomms.core.models.Communication;
import es.com.aaron.tcomms.core.models.Condition;
import es.com.aaron.tcomms.core.models.daos.CommunicationDAO;
import es.com.aaron.tcomms.core.models.daos.ConditionDAO;
import es.com.aaron.tcomms.platform.bootstrap.DIModule;
import es.com.aaron.tcomms.platform.persistence.GenericDAO;
import es.com.aaron.tcomms.platform.persistence.PersistenceService;

/**
 * PersistenceModule class.
 *
 * Class used to configure dependency injection by Guice.
 */
@DIModule
public class PersistenceModule extends AbstractModule {

	@Override
	protected void configure() {
		install(new JpaPersistModule("TCommsMainPU"));
		bind(PersistenceService.class).asEagerSingleton();
		bind(new TypeLiteral<GenericDAO<Communication, Long>>() {}).to(new TypeLiteral<CommunicationDAO>() {});
		bind(new TypeLiteral<GenericDAO<Condition, Long>>() {}).to(new TypeLiteral<ConditionDAO>() {});
	}
}
