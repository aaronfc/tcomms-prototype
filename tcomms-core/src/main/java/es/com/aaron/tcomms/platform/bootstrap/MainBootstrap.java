package es.com.aaron.tcomms.platform.bootstrap;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import org.reflections.Reflections;

import java.util.HashSet;
import java.util.Set;

/**
 * MainBootstrap main entry point to bootstrap all the system, trough injector instantiation.
 */
public class MainBootstrap {

	private static Injector injector;

	/**
	 * Get created injector or instantiate a new one and return it.
	 * @return Injector
	 */
	public static Injector getOrInitiateInjector() {
		if (injector == null) {
			injector = initiateInjector();
		}
		return injector;
	}

	/**
	 * Initiate new injector.
	 * @return Injector created
	 */
	private static Injector initiateInjector() {
		Set<AbstractModule> modules = getAllModules();
		return Guice.createInjector(modules);
	}

	/**
	 * Search all modules annotated with DIModule.
	 *
	 * @return All modules to be loaded.
	 */
	private static Set<AbstractModule> getAllModules() {
		Set<AbstractModule> allModules = new HashSet<AbstractModule>();
		Reflections reflection = new Reflections("es.com.aaron.tcomms");
		Set<Class<?>> modules = reflection.getTypesAnnotatedWith(DIModule.class);
		for(Class<?> module : modules) {
			if (Module.class.isAssignableFrom(module)) {
				try {
					AbstractModule actualModule = (AbstractModule) module.newInstance();
					allModules.add(actualModule);
				} catch (Exception e) {
					throw new RuntimeException("Error trying to load DIModule " + module.getName());
				}
			}
		}
		return allModules;
	}
}
