package es.com.aaron.tcomms.platform.di_modules;

import com.google.inject.AbstractModule;
import es.com.aaron.tcomms.core.templates.TemplateEngine;
import es.com.aaron.tcomms.core.templates.TemplateEngineImpl;
import es.com.aaron.tcomms.core.templates.strategy.TemplatingStrategy;
import es.com.aaron.tcomms.core.templates.strategy.velocity.VelocityTemplatingStrategy;
import es.com.aaron.tcomms.platform.bootstrap.DIModule;

/**
 * TemplateEngineModule class.
 *
 * Class used to configure dependency injection by Guice.
 */
@DIModule
public class TemplateEngineModule extends AbstractModule {
	@Override
	protected void configure() {
		bind(TemplateEngine.class).to(TemplateEngineImpl.class);
		bind(TemplatingStrategy.class).to(VelocityTemplatingStrategy.class);
	}
}
