package es.com.aaron.tcomms.core.models;

import es.com.aaron.tcomms.platform.persistence.AbstractEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Condition entity.
 *
 * This entity represents a condition that can be evaluated. This should contain a condition as a string that will be
 * handled by the effective ConditionEvaluatorStrategy.
 *
 * This are supposed to be reusable by multiple Communications.
 * TODO This relation is now not as supposed to be. One condition will only be used by a unique Communication.
 */
@Entity
@Table(name="`condition`")
@NamedQueries({@NamedQuery(name="Condition.GetAll", query = "from Condition")})
@XmlRootElement
public class Condition extends AbstractEntity<Long> {
	@Id	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String conditionString; // Boolean expression
	@ManyToOne
	@JoinColumn(name="communication_id")
	private Communication communication;

	public Condition() {}

	public Condition(String name, String conditionString) {
		this.name = name;
		this.conditionString = conditionString;
	}

	/**
	 * Get unique identifier for Condition.
	 * @return Unique identifier
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * Get descriptive name for this Condition.
	 * @return Descriptive name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set descriptive name.
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get condition string.
	 * @return Condition string
	 */
	public String getConditionString() {
		return conditionString;
	}

	/**
	 * Set condition string.
	 * @param conditionString
	 */
	public void setConditionString(String conditionString) {
		this.conditionString = conditionString;
	}

	/**
	 * TODO This will be removed
	 * Get Communication this Condition is associated with.
	 * @return Communication
	 */
	public Communication getCommunication() {
		return communication;
	}

	/**
	 * TODO This will be removed
	 * Set Communication.
	 * @param communication
	 */
	public void setCommunication(Communication communication) {
		this.communication = communication;
	}
}
