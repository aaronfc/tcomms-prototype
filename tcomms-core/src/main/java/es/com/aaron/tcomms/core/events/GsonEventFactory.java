package es.com.aaron.tcomms.core.events;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

/**
 * GsonEventFactory class.
 * In charge of the creation of an event instance from a name and the params consisting of a json string.
 */
public class GsonEventFactory implements EventFactory {

	Gson jsonLib;

	@Inject
	public GsonEventFactory(Gson jsonLib) {
		this.jsonLib = jsonLib;
	}

	@Override
	public Event create(String eventName, String jsonParams) throws JsonSyntaxException {
		ParamsBag params = jsonLib.fromJson(jsonParams, ParamsBag.class);
		if (params == null) {
			params = new ParamsBag();
		}
		return new Event(eventName, params);
	}
}
