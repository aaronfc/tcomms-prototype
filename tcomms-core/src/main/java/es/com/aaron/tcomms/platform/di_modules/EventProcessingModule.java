package es.com.aaron.tcomms.platform.di_modules;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import es.com.aaron.tcomms.core.events.*;
import es.com.aaron.tcomms.interfaces.EventListener;
import es.com.aaron.tcomms.platform.bootstrap.DIModule;

/**
 * EventProcessingModule class.
 *
 * Class used to configure dependency injection by Guice.
 */
@DIModule
public class EventProcessingModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(EventFactory.class).to(GsonEventFactory.class).in(Singleton.class);
		bind(EventProcessor.class).to(EventProcessorImpl.class);
		bind(EventListener.class).to(EventListenerImpl.class);
	}
}
