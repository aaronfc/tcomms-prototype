package es.com.aaron.tcomms.platform.config;

/**
 * MainProperties interface.
 */
public interface MainProperties {

    public String getProperty(String propertyName);

    public String getProperty(String propertyName, String defaultValue);

	public void overrideWithFile(String filename);
}
