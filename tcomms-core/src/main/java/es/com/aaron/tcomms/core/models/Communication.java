package es.com.aaron.tcomms.core.models;

import es.com.aaron.tcomms.platform.persistence.AbstractEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

/**
 * Communication entity.
 *
 * This is a communication storage unit. This is the structure defining a communication associated with an Event.
 * A communication might end up delivering a Message to a receiver or not depending on the evaluation of its conditions.
 */
@Entity
@Table(name = "communication")
@NamedQueries({
		@NamedQuery(name = "Communication.GetAll", query = "from Communication"),
		@NamedQuery(name = "Communication.GetByEvent", query = "from Communication WHERE eventName = :event")
})
@XmlRootElement
public class Communication extends AbstractEntity<Long> {

	/*
	 * Entity fields
	 */

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String title;

	private String description;

	@OneToMany(mappedBy = "communication", cascade = CascadeType.ALL)
	private Set<Condition> conditions;

	private String eventName;

	@OneToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="envelopeId")
	private Envelope envelope;

	/*
	 * Entity methods
	 */

	public Communication() {}

	public Communication(String title, String description, String eventName, Envelope envelope) {
		this.title = title;
		this.description = description;
		this.eventName = eventName;
		this.conditions = new HashSet<Condition>();
		this.envelope = envelope;
	}

	/**
	 * Get unique identifier of a Communication
	 * @return Unique identifier of a Communication entry
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * Get descriptive title.
	 * @return Title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set descriptive title.
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Get description.
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set description.
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Get all Conditions associated to this communication.
	 * @return Set of Conditions
	 */
	public Set<Condition> getConditions() {
		return conditions;
	}

	/**
	 * Set conditions.
	 * @param conditions
	 */
	public void setConditions(Set<Condition> conditions) {
		this.conditions = conditions;
	}

	/**
	 * Add extra condition.
	 * @param condition
	 */
	public void addCondition(Condition condition) {
		condition.setCommunication(this);
		this.conditions.add(condition);
	}

	/**
	 * Add extra envelope.
	 * @param envelope
	 * TODO Not used. Remove
	 */
	public void addEnvelope(Envelope envelope) {
		envelope.setCommunication(this);
		this.setEnvelope(envelope);
	}

	/**
	 * Get event name this Communication is associated with.
	 * @return
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * Set event name for this Communication.
	 * @param event
	 */
	public void setEventName(String event) {
		this.eventName = event;
	}

	/**
	 * Set envelope for this Communication.
	 * @param envelope
	 */
	public void setEnvelope(Envelope envelope) {
		this.envelope = envelope;
	}

	/**
	 * Get envelope.
	 * @return envelope
	 */
	public Envelope getEnvelope() {
		return this.envelope;
	}
}
