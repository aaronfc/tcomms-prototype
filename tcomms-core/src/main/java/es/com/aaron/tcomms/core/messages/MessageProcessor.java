package es.com.aaron.tcomms.core.messages;

/**
 * MessageProcessor interface.
 */
public interface MessageProcessor {
	/**
	 * This will process a given communication with the given parameters right now and thus trigger
	 * the delivery of the content to the receiver through the specified sender/s.
	 * @param message Message to be processed
     */
	public void process(Message message);
}
