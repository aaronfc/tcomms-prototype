package es.com.aaron.tcomms.platform.di_modules;

import com.google.inject.AbstractModule;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.core.log.LoggerProviderLog4J2;
import es.com.aaron.tcomms.platform.bootstrap.DIModule;

/**
 * LoggingModule DI module class.
 *
 * Class used to configure dependency injection by Guice.
 */
@DIModule
public class LoggingModule extends AbstractModule {
	@Override
	protected void configure() {
		bind(LoggerProvider.class).to(LoggerProviderLog4J2.class);
	}
}
