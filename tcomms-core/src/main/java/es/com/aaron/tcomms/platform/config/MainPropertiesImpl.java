package es.com.aaron.tcomms.platform.config;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * MainProperties implementation that makes use of *.properties files.
 *
 * This is supposed as the main entry-point for configuration values, it will load at first the default values,
 * later the tcomms.properties specified. And later the values can be overriden on demand.
 */
public class MainPropertiesImpl implements MainProperties {

    private final String PROPERTIES_FILENAME = "/tcomms.properties";
    private final String PROPERTIES_DEFAULT_FILENAME = "/tcomms-default.properties";

    private Logger logger;

    Properties properties;

    @Inject
    public MainPropertiesImpl(LoggerProvider loggerProvider) {
        logger = loggerProvider.getLogger(getClass());
        properties = null;
    }

    @Override
    public String getProperty(String propertyName) {
	    return getProperties().getProperty(propertyName);
    }

    @Override
    public String getProperty(String propertyName, String defaultValue) {
	    return getProperties().getProperty(propertyName, defaultValue);
    }

	@Override
	public void overrideWithFile(String filename) {
		try {
			getProperties().putAll(getPropertiesFromFile(filename));
		} catch (IOException e) {
			logger.error("Configuration file for override \"" + filename + "\" could not be loaded.", e);
		}
	}

	/**
	 * Load Properties object form a file path.
	 * @param filename path to the file
	 * @return Properties object
	 * @throws IOException Throws IOException if file could not be loaded
	 */
    private Properties getPropertiesFromFile(String filename) throws IOException {
	    Properties newProperties = new Properties();
	    InputStream inStream = getClass().getResourceAsStream(filename);
	    if (inStream == null) {
		    throw new IOException();
        }
	    newProperties.load(inStream);
	    return newProperties;
    }

	/**
	 * This will populate the internal properties object first time is called.
	 * @return Properties populated from default configuration files.
	 */
	private Properties getProperties() {
		if (properties == null) {
			properties = new Properties();
			try {
				properties.putAll(getPropertiesFromFile(PROPERTIES_DEFAULT_FILENAME));
			} catch (IOException e) {
				logger.error("Default configuration file \"" + PROPERTIES_DEFAULT_FILENAME + "\" could not be loaded.", e);
			}
			try {
				properties.putAll(getPropertiesFromFile(PROPERTIES_FILENAME));
			} catch (IOException e) {
				logger.info("Custom configuration file \"{}\" could not be loaded.", PROPERTIES_FILENAME);
			}
		}
		return properties;
	}
}
