package es.com.aaron.tcomms.core.exceptions;

/**
 * Exception thrown when a dependency could not be resolved.
 */
public class BrokenDependenciesException extends Exception {
	public BrokenDependenciesException(String msg) {
		super(msg);
	}
}
