package es.com.aaron.tcomms.core.events;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.conditions.ConditionChecker;
import es.com.aaron.tcomms.core.conditions.exceptions.ConditionsEvaluationException;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.core.messages.Message;
import es.com.aaron.tcomms.core.messages.MessageFactory;
import es.com.aaron.tcomms.core.messages.MessageProcessor;
import es.com.aaron.tcomms.core.models.Communication;
import es.com.aaron.tcomms.core.models.daos.CommunicationDAO;
import es.com.aaron.tcomms.core.notifications.EventAction;
import es.com.aaron.tcomms.core.notifications.MessageAction;
import es.com.aaron.tcomms.core.notifications.NotificationsService;
import es.com.aaron.tcomms.core.provisioning.DataProvisionService;
import es.com.aaron.tcomms.core.provisioning.extractors.ConditionParamsExtractor;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import es.com.aaron.tcomms.platform.config.MainProperties;
import es.com.aaron.tcomms.platform.config.QueuesServiceConfig;
import es.com.aaron.tcomms.platform.queues.QueuesService;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Events processor class.
 *
 * This class is the responsible of processing a received event.
 */
public class EventProcessorImpl implements EventProcessor {

	private final Logger logger;
	private DataProvisionService provisionService;
	private CommunicationDAO communicationDao;
	private ConditionChecker conditionChecker;
	private MessageProcessor messageProcessor;
	private ConditionParamsExtractor conditionParamsExtractor;
	private NotificationsService actionsObserver;
	private MessageFactory messageFactory;
	private MainProperties mainProperties;
	private QueuesService queuesService;
	private QueuesServiceConfig queuesConfig;

	@Inject
	public EventProcessorImpl(DataProvisionService provisionService, CommunicationDAO communicationDao,
	                          ConditionChecker conditionChecker, MessageProcessor messageProcessor,
	                          ConditionParamsExtractor conditionParamsExtractor,
	                          NotificationsService actionsObserver, MessageFactory messageFactory,
	                          LoggerProvider loggerProvider, MainProperties mainProperties,
	                          QueuesService queuesService, QueuesServiceConfig queuesConfig) {
		this.provisionService = provisionService;
		this.communicationDao = communicationDao;
		this.conditionChecker = conditionChecker;
		this.messageProcessor = messageProcessor;
		this.conditionParamsExtractor = conditionParamsExtractor;
		this.actionsObserver = actionsObserver;
		this.messageFactory = messageFactory;
		this.mainProperties = mainProperties;
		this.queuesService = queuesService;
		this.queuesConfig = queuesConfig;
		this.logger = loggerProvider.getLogger(getClass());
	}

	@Override
	public void onEvent(Event event) {
		try {
			actionsObserver.notifyEventAction(event, EventAction.START);
			List<Communication> allCommunications = this.getCommunicationsForEvent(event);
			Set<String> conditionParamKeys = conditionParamsExtractor.extractFromAllCommunications(allCommunications);
			ParamsBag conditionParams = provisionService.getParameters(event.getParams(), conditionParamKeys);
			List<Communication> communications = getFilteredComms(event, conditionParams);
			for (Communication communication : communications) {
				final Message message = messageFactory.create(event, communication);
				actionsObserver.notifyMessageAction(message, MessageAction.CREATE);

				// TODO Move this logic into QueuesService.
				Boolean areQueuesEnabled = Boolean.valueOf(mainProperties.getProperty("tcomms.queues.enabled"));
				// Enqueue event
				if (areQueuesEnabled) {
					queuesService.enqueue(queuesConfig.getProcessesQueueName(), message);
				} else {
					// Execute in new thread
					new Thread(new Runnable() {
						public void run() {
							messageProcessor.process(message);
						}
					}).start();
				}

			}
			actionsObserver.notifyEventAction(event, EventAction.COMPLETE);
		} catch (Exception e) {
			actionsObserver.notifyEventException(event, e);
			logger.error("Exception when processing an event.", e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Retrieve all communications for the given event and filter out the ones whose conditions could not be asserted.
	 *
	 * @param event Event
	 * @param params Bag of parameters for conditions evaluation
	 * @return Final list of communications
	 */
	private List<Communication> getFilteredComms(Event event, ParamsBag params) {
		List<Communication> communications = new ArrayList<Communication>();
		List<Communication> allCommunications = communicationDao.getAllByEvent(event);
		for (Communication communication :  allCommunications) {
			try {
				if (conditionChecker.evaluate(communication.getConditions(), params)) {
					communications.add(communication);
				}
			} catch (ConditionsEvaluationException e) {
				logger.error("Exception when evaluation conditions for a communication", e);
			}
		}
		return communications;
	}

	/**
	 * Get all communications for an event.
	 * @param event Event
	 * @return All communications for given event.
	 */
	private List<Communication> getCommunicationsForEvent(Event event) {
		return  communicationDao.getAllByEvent(event);
	}
}
