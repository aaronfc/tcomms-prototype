package es.com.aaron.tcomms.core.templates;

import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import es.com.aaron.tcomms.interfaces.senders.SenderFields;

/**
 * TemplateEngine interface.
 */
public interface TemplateEngine {
	/**
	 * Will generate SenderFields whose attributes have already been processed
	 * making use of the given Params.
	 * @param senderFields Input SenderFields parameter.
	 * @param params Data to be used for parsing.
	 * @return Input senderFields copy with parsed attributes
	 */
	public SenderFields process(SenderFields senderFields, ParamsBag params);
}
