package es.com.aaron.tcomms.core.provisioning.extractors;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.conditions.strategy.ConditionEvaluatorStrategy;
import es.com.aaron.tcomms.core.models.Condition;
import es.com.aaron.tcomms.core.models.Communication;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Condition parameters extractor.
 *
 * This is the implementation of a ParamsExtractor that is supposed to be used when extracting the parameters
 * for a Condition string.
 */
public class ConditionParamsExtractor implements ParamsExtractor {

	ConditionEvaluatorStrategy strategy;

	@Inject
	public ConditionParamsExtractor(ConditionEvaluatorStrategy conditionEvaluatorStrategy) {
		this.strategy = conditionEvaluatorStrategy;

	}

	@Override
	public Set<String> extract(Object... inputObjects) {
		Set<String> keySet = new HashSet<String>();
		for (Object inputObject : inputObjects) {
			if (inputObject instanceof Condition) {
				Condition condition = (Condition) inputObject;
				Set<String> params = strategy.parse(condition.getConditionString());
				for(String paramPath : params) {
					keySet.add(paramPath);
				}
			}
		}
		return keySet;
	}

	/**
	 * Extract all parameters for a list of Communications from all of its Conditions.
	 * @param communications Communications to be parsed
	 * @return Set of parameter keys used on those Communication's conditions.
	 */
    public Set<String> extractFromAllCommunications(List<Communication> communications) {
        Set<String> requiredParamsKeySet = new HashSet<String>();
        for (Communication communication : communications) {
            Set<String> keySet = this.extract(communication.getConditions().toArray());
            requiredParamsKeySet.addAll(keySet);
        }
        return requiredParamsKeySet;
    }
}
