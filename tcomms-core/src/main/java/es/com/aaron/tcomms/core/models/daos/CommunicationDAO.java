package es.com.aaron.tcomms.core.models.daos;

import es.com.aaron.tcomms.core.events.Event;
import es.com.aaron.tcomms.core.models.Communication;
import es.com.aaron.tcomms.platform.persistence.GenericDAOImpl;

import java.util.List;

/**
 * CommunicationDAO class.
 */
public class CommunicationDAO extends GenericDAOImpl<Communication, Long> {

	/**
	 * Get all communications matching a given event
	 * @param event Event to match against
	 * @return List of communications
	 */
	public List<Communication> getAllByEvent(Event event) {
		return this.getQuery("GetByEvent").setParameter("event", event.getName()).getResultList();
	}

}
