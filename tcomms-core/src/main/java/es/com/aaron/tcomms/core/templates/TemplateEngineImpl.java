package es.com.aaron.tcomms.core.templates;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.templates.strategy.TemplatingStrategy;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import es.com.aaron.tcomms.interfaces.senders.SenderFields;

/**
 * TemplateEngineImpl class implementing TemplateEngine.
 *
 * Will be used to parse all sender fields with the given parameters.
 */
public class TemplateEngineImpl implements TemplateEngine {

	TemplatingStrategy templatingStrategy;

	@Inject
	public TemplateEngineImpl(TemplatingStrategy templatingStrategy) {
		this.templatingStrategy = templatingStrategy;
	}

	public SenderFields process(SenderFields senderFields, ParamsBag params) {
		SenderFields finalFields = new SenderFields();
		for(String key : senderFields.keySet()) {
			// TODO Possible exception to be thrown if parameters does not match expectations when rendering
			String finalString = templatingStrategy.render(senderFields.get(key), params);
			finalFields.put(key, finalString);
		}
		return finalFields;
	}
}
