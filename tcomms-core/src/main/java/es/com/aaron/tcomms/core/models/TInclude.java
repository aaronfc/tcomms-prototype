package es.com.aaron.tcomms.core.models;

import es.com.aaron.tcomms.platform.persistence.AbstractEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TInclude entity.
 *
 * This is used as dynamic content to be included in the communication templates. Its most common usages will be
 * to set a default header or footer for communications.
 */
@Entity
@Table(name = "tinclude", uniqueConstraints = @UniqueConstraint(columnNames = "title"))
@NamedQueries({
		@NamedQuery(name = "TInclude.GetAll", query = "from TInclude order by id ASC")
})
@XmlRootElement
public class TInclude extends AbstractEntity<Long> {
	/*
	 * Entity fields
	 */
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	// TODO Rename this to name
	@Column(unique=true, nullable=false)
	private String title;

	// TODO Review length this needs to be "unlimited"
	private String content;

	/*
	 * Entity methods
	 */

	public TInclude() {}

	public TInclude(String title, String content) {
		this.title = title;
		this.content = content;
	}

	/**
	 * Get unique identifier.
	 * @return Unique identifier
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * Get the title. This will be used to access to them from.
	 * @return Title of the TInclude
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set TInclude title.
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Get content. Can contain constructions to be handled by the template engine.
	 * @return String with the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Set content. Can contain constructions to be handled by the template engine.
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}
}
