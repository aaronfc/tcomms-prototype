package es.com.aaron.tcomms.core.provisioning;

import com.google.inject.Inject;
import com.google.inject.Injector;
import es.com.aaron.tcomms.core.exceptions.BrokenDependenciesException;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.core.provisioning.resolvers.ParametersResolverStrategy;
import es.com.aaron.tcomms.interfaces.data_providers.DataProvider;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import es.com.aaron.tcomms.platform.config.MainProperties;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**
 * DataProvisionService implementation.
 *
 * This will use the MainProperties class to auto-discover the data providers or, if it is not the case, try to load one
 * by one the enlisted ones. This will use the strategy defined in the DataProvisionModule. But this decision may end-up
 * being in configuration.
 */
public class DataProvisionServiceImpl implements DataProvisionService {

	private final Logger logger;
	private Injector injector;
	private MainProperties mainProperties;
	ParametersResolverStrategy resolverStrategy;

	@Inject
	public DataProvisionServiceImpl(Injector injector, MainProperties mainProperties,
	                                ParametersResolverStrategy resolverStrategy, LoggerProvider loggerProvider) {
		this.injector = injector;
		this.mainProperties = mainProperties;
		this.resolverStrategy = resolverStrategy;
		this.logger = loggerProvider.getLogger(getClass());
	}

	/**
	 * Return resolved parameters with their value.
	 * @param inputParams Input parameters
	 * @param requiredParamsKeySet Required parameters set
	 * @return Set of parameters generated
	 */
	public ParamsBag getParameters(ParamsBag inputParams, Set<String> requiredParamsKeySet) throws BrokenDependenciesException {
		return this.resolverStrategy.resolveParameters(inputParams, getAllDataProviders(), requiredParamsKeySet);
	}

	/**
	 * Get all data providers available. New instances are created on every call.
	 * Maybe this should be cached in a static Set<DataProvider>.
	 * @return List of DataProvider available
	 */
	private Set<DataProvider> getAllDataProviders() {
		Set<DataProvider> set = new HashSet<DataProvider>();

		// TODO This logic may be moved to a classes crawler class.
		Boolean autodiscovery = Boolean.valueOf(mainProperties.getProperty("tcomms.data_providers.autodiscovery", "false"));
		if (autodiscovery) {
			logger.error("Auto-discovery for DataProviders is not implemented yet.");
		} else {
			// We use here Guice. External dataproviders should use Guice for injection or allow an empty constructor.
			String providersString = mainProperties.getProperty("tcomms.data_providers", "");
			String[] classes = providersString.split(",");
			for(String clazzString : classes) {
				try {
					Class clazz = Class.forName(clazzString.trim());
					set.add((DataProvider) injector.getInstance(clazz));
				} catch (ClassNotFoundException e) {
					logger.error(String.format("Could not instantiate data provider \"%s\". Class not found.", clazzString), e);
				}
			}
		}

		return set;
	}
}
