package es.com.aaron.tcomms.platform.config;

/**
 * QueuesServiceConfig implementation.
 *
 * This is currently hardcoded as queues are a feature under development.
 * TODO Implement this or remove the feature
 */
public class QueuesServiceConfigImpl implements QueuesServiceConfig {
    @Override
    public String getHost() {
        return "localhost";
    }

    @Override
    public String getEventQueueName() {
        return "EVENTS_QUEUE";
    }

    @Override
    public String getProcessesQueueName() {
        return "PROCESSES_QUEUE";
    }

}
