package es.com.aaron.tcomms.core.conditions;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.conditions.exceptions.ConditionsEvaluationException;
import es.com.aaron.tcomms.core.conditions.strategy.ConditionEvaluatorStrategy;
import es.com.aaron.tcomms.core.conditions.strategy.exceptions.UnavailableVariableException;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.core.models.Condition;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import org.apache.logging.log4j.Logger;

import java.util.Set;

/**
 * Implementation of ConditionChecker.
 */
public class ConditionCheckerImpl implements ConditionChecker {

	private final Logger logger;
	private ConditionEvaluatorStrategy evaluator;

	@Inject
	public ConditionCheckerImpl(ConditionEvaluatorStrategy evaluator, LoggerProvider loggerProvider) {
		this.evaluator = evaluator;
		this.logger = loggerProvider.getLogger(getClass());
	}

	public boolean evaluate(Set<Condition> conditions, ParamsBag params) throws ConditionsEvaluationException {
		for (Condition condition : conditions) {
			try {
				if (!evaluator.check(condition.getConditionString(), params)) {
					return false;
				}
			} catch (UnavailableVariableException e) {
				logger.error("Conditions could not be evaluated because a parameter was missing.", e);
				throw new ConditionsEvaluationException(condition, params);
			}
		}
		return true;
	}
}