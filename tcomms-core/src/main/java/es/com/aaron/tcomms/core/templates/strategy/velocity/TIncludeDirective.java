package es.com.aaron.tcomms.core.templates.strategy.velocity;

import com.google.inject.Injector;
import es.com.aaron.tcomms.core.models.TInclude;
import es.com.aaron.tcomms.core.models.daos.TIncludeDAO;
import es.com.aaron.tcomms.core.templates.strategy.TemplatingStrategy;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import org.apache.velocity.context.Context;
import org.apache.velocity.context.InternalContextAdapter;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.directive.Directive;
import org.apache.velocity.runtime.parser.node.Node;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * TIncludeDirective class.
 *
 * This is a custom Velocity's directive in order to allow includes loaded from database.
 * It expects dependency injector to be present as an application attribute in the RuntimeServices which should have
 * been set up when instantiating the engine in VelocityTemplatingStrategy.
 */
public class TIncludeDirective extends Directive {
	@Override
	public String getName() {
		return "tinclude";
	}

	@Override
	public int getType() {
		return LINE;
	}

	@Override
	public boolean render(InternalContextAdapter internalContextAdapter, Writer writer, Node node) throws IOException, ResourceNotFoundException, ParseErrorException, MethodInvocationException {
		Injector injector = (Injector) rsvc.getApplicationAttribute("di_injector");
		TIncludeDAO tIncludeDAO = injector.getInstance(TIncludeDAO.class);
		TemplatingStrategy templatingStrategy = injector.getInstance(TemplatingStrategy.class);
		ParamsBag paramsBag = this.getParamsBagFromContext(internalContextAdapter);
		Map<String, TInclude> tIncludes = tIncludeDAO.getAllAsMap();
		for (int i=0; i < node.jjtGetNumChildren(); i++) {
			String includeName = String.valueOf(node.jjtGetChild(i).value(internalContextAdapter));
			if (tIncludes.containsKey(includeName)) {
				writer.write(templatingStrategy.render(tIncludes.get(includeName).getContent(), paramsBag));
			}
		}
		return true;
	}

	/**
	 * This will generate a ParamsBag from a given context content.
	 * @param context containing the info needed for parsing
	 * @return ParamsBag instance
	 */
	private ParamsBag getParamsBagFromContext(Context context) {
		ParamsBag paramsBag = new ParamsBag();
		for(Object key : context.getKeys()) {
			paramsBag.put((String) key, context.get((String) key));
		}
		return paramsBag;
	}
}
