package es.com.aaron.tcomms.core.provisioning.resolvers;

import es.com.aaron.tcomms.core.utils.ParameterUtils;
import es.com.aaron.tcomms.interfaces.data_providers.DataProvider;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import java.util.*;

/**
 * This parameters collection strategy will retrieve all parameters available for the given input
 * parameters. This will also use as input for data providers the output of the others trying to solve collision of
 * parameters with the same name by itself.
 *
 * DISCLAIMER: This is highly experimental. Please do not use it unless you are sure about it. It could generate
 * inconsistent parameters if DataProviders are not correctly created.
 */
public class ExperimentalParametersResolver implements ParametersResolverStrategy {

	@Override
	public ParamsBag resolveParameters(ParamsBag inputParams, Set<DataProvider> providers, Set<String> required) {
		ParamsBag output = new ParamsBag();
		// Add all input parameters, they are always expected on the output
		output.putAll(inputParams);

		Map<String, Set<DataProvider>> dependenciesMap = getDependenciesMap(inputParams, providers, required);

		// TODO Not implemented yet!


		// With inputparams check all required dataproviders
		// Set input parameters
		return output;
	}

	private Map<String, Set<DataProvider>> getDependenciesMap(ParamsBag inputParams, Set<DataProvider> providers, Set<String> allRequired) {

		Map<String, Set<DataProvider>> availabilityMap = new HashMap<String, Set<DataProvider>>();
		for(String required : allRequired) {
			Set<DataProvider> matches = new HashSet<DataProvider>();
			if (ParameterUtils.isPrefixed(required)) {

			}
			for(DataProvider provider : providers) {
				if (provider.getAllParametersKeys().contains(required)) {
					matches.add(provider);
				}
			}

		}
		return null;
	}
}