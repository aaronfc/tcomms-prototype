package es.com.aaron.tcomms.core.events;

/**
 * EventFactory interface.
 */
public interface EventFactory {
	/**
	 * Create an event entity by the name and its parameters as a JSON string.
	 * @param eventName String used as event name
	 * @param params String with the parameters as a JSON
	 * @return Event entity
	 */
	public Event create(String eventName, String params);
}
