package es.com.aaron.tcomms.core.messages;

import es.com.aaron.tcomms.core.events.Event;
import es.com.aaron.tcomms.core.models.Communication;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import es.com.aaron.tcomms.interfaces.senders.SenderFields;

import java.util.UUID;

/**
 * Message entity.
 *
 * This entity contains all data a communication being processed consists of.
 * This is in fact the combination of an Event (with its input parameters) and a Communication (a template text to be
 * sent through a Sender to a receiver).
 */
public class Message {
	private UUID uuid;
	private Communication communication;
	private Event event;

	/*
	 * Optional parameters set during Message lifecycle.
	 */
	private SenderFields processedSenderFields = null;
	private ParamsBag renderingParams;
	private SenderFields renderedFields;

	public Message(Event event, Communication communication) {
		this.uuid = UUID.randomUUID();
		this.communication = communication;
		this.event = event;
	}

	/**
	 * Get unique identifier
	 * @return
	 */
	public UUID getUuid() {
		return uuid;
	}

	/**
	 * Set unique identifier
	 * @param uuid
	 */
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	/**
	 * Get communication
	 * @return
	 */
	public Communication getCommunication() {
		return communication;
	}

	/**
	 * Set communication
	 * @param communication
	 */
	public void setCommunication(Communication communication) {
		this.communication = communication;
	}

	/**
	 * Get event
	 * @return
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * Set event
	 * @param event
	 */
	public void setEvent(Event event) {
		this.event = event;
	}

	/**
	 * Set already processed sender fields
	 * @param processedSenderFields
	 */
	public void setProcessedSenderFields(SenderFields processedSenderFields) {
		this.processedSenderFields = processedSenderFields;
	}

	/**
	 * Set rendering parameters
	 * @param renderingParams
	 */
	public void setRenderingParams(ParamsBag renderingParams) {
		this.renderingParams = renderingParams;
	}

	/**
	 * Set already rendered parameters
	 * @param renderedFields
	 */
	public void setRenderedFields(SenderFields renderedFields) {
		this.renderedFields = renderedFields;
	}

	/**
	 * Get processed sender fields
	 * @return
	 */
	public SenderFields getProcessedSenderFields() {
		return processedSenderFields;
	}

	/**
	 * Get rendering parameters
	 * @return
	 */
	public ParamsBag getRenderingParams() {
		return renderingParams;
	}

	/**
	 * Get already rendered fields
	 * @return
	 */
	public SenderFields getRenderedFields() {
		return renderedFields;
	}

	@Override
	public String toString() {
		return "Message{" +
				"uuid=" + uuid +
				", communication=" + communication +
				", event=" + event +
				", processedSenderFields=" + processedSenderFields +
				", renderingParams=" + renderingParams +
				", renderedFields=" + renderedFields +
				'}';
	}
}
