package es.com.aaron.tcomms.core.conditions.exceptions;

import es.com.aaron.tcomms.core.models.Condition;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

/**
 * ConditionsEvaluationException class.
 *
 * Thrown when a condition could not be evaluated with the given parameters.
 */
public class ConditionsEvaluationException extends Exception {
	public ConditionsEvaluationException(Condition condition, ParamsBag params) {
		super("Not evaluable condition: \"" + condition.getConditionString()
				+ "\" with params: \"" + params.toString() + "\"");
	}
}
