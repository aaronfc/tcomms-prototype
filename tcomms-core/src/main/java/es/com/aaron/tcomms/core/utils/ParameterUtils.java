package es.com.aaron.tcomms.core.utils;

import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import org.apache.commons.lang.StringUtils;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * ParameterUtils class.
 */
public class ParameterUtils {
	public static final String SEPARATOR = ".";

	/**
	 * Check if a parameter is a prefixed parameter.
	 * @param key
	 * @return Whether the parameter has a prefix or not
	 */
	public static boolean isPrefixed(String key) {
		return key.contains(SEPARATOR);
	}

	/**
	 * Get prefix from a parameter
	 * @param key
	 * @return The prefix or null
	 */
	public static String getPrefix(String key) {
		if (!isPrefixed(key)) {
			return "";
		}
		String[] pieces = key.split(Pattern.quote(SEPARATOR), 2);
		return pieces[0];
	}

	/**
	 * Get prefix from a parameter
	 * @param key
	 * @return The name of the key without the prefix (if any)
	 */
	public static String getSimpleName(String key) {
		if (!isPrefixed(key)) {
			return key;
		}
		String[] pieces = key.split(Pattern.quote(SEPARATOR), 2);
		return pieces[1];
	}

	/**
	 * Extract parameters from a ParamsBag that are prefixed with a given string into a ParamsBag without the prefix.
	 * @param params Input parameters
	 * @param prefix String used as prefix
	 * @return Output parameters
	 */
	public static ParamsBag getPrefixedParams(ParamsBag params, String prefix) {
		ParamsBag out = new ParamsBag();
		if (params.containsKey(prefix)) {
			out.putAll((Map) params.get(prefix));
		}
		return out;
	}

	/**
	 * Filter parameter keys that are prefixed with a given string. Keys are returned without the prefix.
	 * @param keys Parameter keys
	 * @param prefix String used as prefix
	 * @return Output parameters
	 */
	public static Set<String> getPrefixedParamKeys(Set<String> keys, String prefix) {
		Set<String> out = new HashSet<String>();
		for(String key : keys) {
			if (getPrefix(key).equals(prefix)) {
				out.add(getSimpleName(key));
			}
		}
		return out;
	}

	/**
	 * Generate a composed key by its pieces
	 * @param pieces
	 * @return
	 */
	public static String generateKey(String ... pieces) {
		return StringUtils.join(pieces, SEPARATOR);
	}
}
