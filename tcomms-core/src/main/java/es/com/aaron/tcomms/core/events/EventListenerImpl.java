package es.com.aaron.tcomms.core.events;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.core.notifications.EventAction;
import es.com.aaron.tcomms.core.notifications.NotificationsService;
import es.com.aaron.tcomms.interfaces.EventListener;
import es.com.aaron.tcomms.platform.config.MainProperties;
import es.com.aaron.tcomms.platform.config.QueuesServiceConfig;
import es.com.aaron.tcomms.platform.queues.QueuesService;
import org.apache.logging.log4j.Logger;

/**
 * EventListener implementation.
 *
 * This is the implementation of EventListener to be used.
 * This will use MainProperties and QueuesServiceConfig to enqueue (if enabled) the processing of a created event.
 */
public class EventListenerImpl implements EventListener {

	private final Logger logger;
	private final MainProperties mainProperties;
	private final QueuesServiceConfig queuesConfig;
	private EventFactory eventFactory;
	private EventProcessor eventProcessor;
	private NotificationsService actionsObserver;
	private QueuesService queuesService;

	@Inject
	public EventListenerImpl(MainProperties mainProperties, QueuesServiceConfig queuesServiceConfig,
	                         QueuesService queuesService, EventFactory eventFactory, EventProcessor eventProcessor,
	                         NotificationsService actionsObserver, LoggerProvider loggerProvider) {
		this.mainProperties = mainProperties;
		this.eventFactory = eventFactory;
		this.eventProcessor = eventProcessor;
		this.actionsObserver = actionsObserver;
		this.queuesService = queuesService;
		this.logger = loggerProvider.getLogger(getClass());
		this.queuesConfig = queuesServiceConfig;
	}

	@Override
	public Boolean handleEvent(String eventName, String eventInput) {

		logger.debug("Received event name: " + eventName);
		logger.debug("Received event parameters: " + eventInput);

		// TODO Logic to verify event seems valid would be appreciated
		final Event event = eventFactory.create(eventName, eventInput);
		actionsObserver.notifyEventAction(event, EventAction.CREATE);

		// TODO Move this logic into QueuesService.
		Boolean areQueuesEnabled = Boolean.valueOf(mainProperties.getProperty("tcomms.queues.enabled"));
		// Enqueue event
		if (areQueuesEnabled) {
			queuesService.enqueue(queuesConfig.getEventQueueName(), event);
		} else {
			// Execute in new thread
			new Thread(new Runnable() {
				public void run() {
					eventProcessor.onEvent(event);
				}
			}).start();
		}

		return Boolean.TRUE;
	}
}
