package es.com.aaron.tcomms.core.provisioning.resolvers;

import es.com.aaron.tcomms.core.exceptions.BrokenDependenciesException;
import es.com.aaron.tcomms.interfaces.data_providers.DataProvider;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import java.util.Set;

/**
 * Parameters resolver strategy interface.
 */
public interface ParametersResolverStrategy {
	public ParamsBag resolveParameters(ParamsBag inputParams, Set<DataProvider> providers, Set<String> required) throws BrokenDependenciesException;
}
