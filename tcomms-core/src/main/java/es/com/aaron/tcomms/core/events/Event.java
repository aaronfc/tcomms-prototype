package es.com.aaron.tcomms.core.events;

import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import java.util.UUID;

/**
 * Event entity.
 *
 * This entity represents a unique event and all its content.
 */
public class Event {
    private UUID uuid;
	private String name;
	private ParamsBag params;

	public Event(String name, ParamsBag params) {
        this.uuid = UUID.randomUUID();
		this.name = name;
		this.params = params;
	}

	/**
	 * Get parameters.
	 * @return parameters
	 */
	public ParamsBag getParams() {
		return params;
	}

	/**
	 * Get name.
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get unique identifier
	 * @return uuid
	 */
    public UUID getUuid() { return uuid; }

    @Override
    public String toString() {
        return "Event{" +
                "uuid=" + uuid +
                ", name='" + name + '\'' +
                ", params=" + params +
                '}';
    }
}
