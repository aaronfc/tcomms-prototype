package es.com.aaron.tcomms.core.models;

import es.com.aaron.tcomms.platform.persistence.AbstractEntity;
import es.com.aaron.tcomms.interfaces.senders.Sender;
import es.com.aaron.tcomms.interfaces.senders.SenderFields;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by aaron on 13/12/13.
 */
@Entity
@Table(name="sender_envelope")
//@NamedQueries({@NamedQuery(name="Envelope.GetByCommunicationId", query = "from Envelope where communication_id=:commId")})
@XmlRootElement
public class Envelope extends AbstractEntity<Long> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Class<Sender> senderClass;
	@Lob
	private SenderFields data; // JSON?
	@OneToOne(fetch=FetchType.LAZY, mappedBy="envelope")
	private Communication communication;

	// Required at some point by jersey
	// TODO Look into this
	public Envelope() {}

	public Envelope(Class senderClass, SenderFields data) {
		this.senderClass = senderClass;
		this.data = data;
	}

	@Override
	public Long getId() {
		return id;
	}

	public Class<Sender> getSenderClass() {
		return senderClass;
	}

	public void setSenderClassName(Class<Sender> senderClass) {
		this.senderClass = senderClass;
	}

	public SenderFields getSenderData() {
		return data;
	}

	public void setSenderData(SenderFields data) {
		this.data = data;
	}

	public Communication getCommunication() {
		return communication;
	}

	public void setCommunication(Communication communication) {
		this.communication = communication;
	}
}
