package es.com.aaron.tcomms.core.notifications;

/**
* Actions available to be logged for Events.
*/
public enum EventAction {
    START,
    CREATE,
	COMPLETE
}
