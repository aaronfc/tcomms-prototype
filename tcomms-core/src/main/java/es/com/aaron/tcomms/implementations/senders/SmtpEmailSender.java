package es.com.aaron.tcomms.implementations.senders;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.interfaces.senders.Sender;
import es.com.aaron.tcomms.interfaces.senders.SenderFields;
import es.com.aaron.tcomms.platform.config.MainProperties;
import org.apache.logging.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

/**
 * EmailSender class.
 *
 * Sender that will allow the delivery of messages as email through SMTP server.
 */
public class SmtpEmailSender implements Sender {

	// Localhost is set by default. FakeSMTP will be used for testing.
	// See http://nilhcem.github.io/FakeSMTP
	private final String DEFAULT_HOST = "localhost";
	private final String DEFAULT_PORT = "2525";
	private final String DEFAULT_AUTH = "false";
	private final String DEFAULT_USER = "";
	private final String DEFAULT_PASSWORD = "";

	/*
	 * Dependencies
	 */

	private MainProperties mainProperties;
	private final Logger logger;

	@Inject
	public SmtpEmailSender(MainProperties mainProperties, LoggerProvider loggerProvider) {
		this.mainProperties = mainProperties;
		this.logger = loggerProvider.getLogger(getClass());
	}

	@Override
	public Set<String> getRequiredFields() {
		return new HashSet<String>(Arrays.asList("from", "to", "subject", "body"));
	}

	@Override
	public void send(SenderFields fields) {
		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host",
				mainProperties.getProperty("tcomms.senders.email.smtp.host", DEFAULT_HOST));
		properties.setProperty("mail.smtp.port",
				mainProperties.getProperty("tcomms.senders.email.smtp.port", DEFAULT_PORT));
		String auth = mainProperties.getProperty("tcomms.senders.email.smtp.auth", DEFAULT_AUTH);
		properties.setProperty("mail.smtp.auth", auth);

		// Create authenticator if required
		Authenticator authenticator = null;
		if (auth.toLowerCase().equals("true")) {
			authenticator = new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(
							mainProperties.getProperty("tcomms.senders.email.smtp.user", DEFAULT_USER),
							mainProperties.getProperty("tcomms.senders.email.smtp.password", DEFAULT_PASSWORD)
					);
				}
			};
		}
		// Instantiate default session
		Session session = Session.getDefaultInstance(properties, authenticator);

		try {
			// Create MimeMessage object.
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fields.get("from")));
			message.addRecipient(Message.RecipientType.TO,
					new InternetAddress(fields.get("to")));
			message.setSubject(fields.get("subject"));
			message.setText(fields.get("body"));

			// Actual delivery of the message
			Transport.send(message);
			this.logger.debug("Email sent successfully to the SMTP server.");
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}
