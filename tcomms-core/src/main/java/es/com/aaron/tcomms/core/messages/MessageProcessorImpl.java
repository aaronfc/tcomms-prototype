package es.com.aaron.tcomms.core.messages;

import com.google.inject.Inject;
import com.google.inject.Injector;
import es.com.aaron.tcomms.core.events.Event;
import es.com.aaron.tcomms.core.exceptions.BrokenDependenciesException;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.core.models.Communication;
import es.com.aaron.tcomms.core.models.Envelope;
import es.com.aaron.tcomms.core.notifications.MessageAction;
import es.com.aaron.tcomms.core.notifications.NotificationsService;
import es.com.aaron.tcomms.core.provisioning.DataProvisionService;
import es.com.aaron.tcomms.core.provisioning.extractors.SenderFieldsParamsExtractor;
import es.com.aaron.tcomms.core.templates.TemplateEngine;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;
import es.com.aaron.tcomms.interfaces.senders.Sender;
import es.com.aaron.tcomms.interfaces.senders.SenderFields;
import org.apache.logging.log4j.Logger;

import java.util.Set;

/**
 * MessageProcessor implementation.
 *
 * This is the class in charge of processing a Message. It will retrieve rendering parameters, render the final text
 * and then send it through the specified Sender.
 */
public class MessageProcessorImpl implements MessageProcessor {

	private final Logger logger;
	private Injector injector;

	private TemplateEngine templateEngine;
	private DataProvisionService provisionService;
	private SenderFieldsParamsExtractor senderFieldsParamsExtractor;
	private NotificationsService actionsObserver;

	@Inject
	public MessageProcessorImpl(Injector injector, TemplateEngine templateEngine, DataProvisionService provisionService,
	                            SenderFieldsParamsExtractor senderFieldsParamsExtractor,
	                            NotificationsService actionsObserver, LoggerProvider loggerProvider) {
		this.injector = injector;
		this.templateEngine = templateEngine;
		this.provisionService = provisionService;
		this.senderFieldsParamsExtractor = senderFieldsParamsExtractor;
		this.actionsObserver = actionsObserver;
		this.logger = loggerProvider.getLogger(getClass());
	}

	@Override
	public void process(Message message) {
		try {
			actionsObserver.notifyMessageAction(message, MessageAction.STARTED);
			Communication communication = message.getCommunication();
			Event event = message.getEvent();
			Envelope envelope = communication.getEnvelope();

			// Retrieve parameters for rendering
			ParamsBag renderParams = getRenderingParams(communication, event.getParams());
			setRenderingParamsOnMessage(message, renderParams);

			// Generate rendered data fields
			SenderFields renderedDataFields = getRenderedDataFields(envelope.getSenderData(), renderParams);
			setRenderedFieldsOnMessage(message, renderedDataFields);

			// Send to sender
			Sender sender = injector.getInstance(envelope.getSenderClass());
			sender.send(renderedDataFields);
			actionsObserver.notifyMessageAction(message, MessageAction.COMPLETED);

		} catch (Exception e) {
			actionsObserver.notifyMessageException(message, e);
			logger.error("Exception when processing a message", e);
			// TODO Re-enqueue?
		}
	}

	/**
	 * Retrieve parameters for final text rendering.
	 * @param communication Communication to be rendered
	 * @param inputParams Input parameters
	 * @return Bag of final parameters
	 * @throws BrokenDependenciesException
	 */
	private ParamsBag getRenderingParams(Communication communication, ParamsBag inputParams) throws BrokenDependenciesException {
		Envelope envelope = communication.getEnvelope();
		Set<String> parameters = senderFieldsParamsExtractor.extract(envelope.getSenderData());
		return provisionService.getParameters(inputParams, parameters);
	}

	/**
	 * Update message with the redering parameters.
	 * @param message Message to update
	 * @param renderParams Params bag
	 */
	private void setRenderingParamsOnMessage(Message message, ParamsBag renderParams) {
		message.setRenderingParams(renderParams);
		actionsObserver.notifyMessageAction(message, MessageAction.RENDERING_PARAMS_AVAILABLE);
	}

	/**
	 * Get final texts rendered with a bag of parameters.
	 * @param original Original texts (templates)
	 * @param renderParameters Bag of parameters to be used for rendering
	 * @return Rendered texts
	 */
	private SenderFields getRenderedDataFields(SenderFields original, ParamsBag renderParameters) {
		return templateEngine.process(original, renderParameters);
	}

	/**
	 * Update message with rendered fields
	 * @param message Message to update
	 * @param rendered Rendered fields
	 */
	private void setRenderedFieldsOnMessage(Message message, SenderFields rendered) {
		message.setRenderedFields(rendered);
		actionsObserver.notifyMessageAction(message, MessageAction.RENDERED_FIELDS_AVAILABLE);
	}
}
