package es.com.aaron.tcomms.platform.di_modules;

import com.google.inject.AbstractModule;
import es.com.aaron.tcomms.core.conditions.ConditionChecker;
import es.com.aaron.tcomms.core.conditions.ConditionCheckerImpl;
import es.com.aaron.tcomms.core.conditions.strategy.ConditionEvaluatorStrategy;
import es.com.aaron.tcomms.core.conditions.strategy.MVELConditionEvaluatorStrategy;
import es.com.aaron.tcomms.platform.bootstrap.DIModule;

/**
 * ConditionsModule class.
 *
 * Class used to configure dependency injection by Guice.
 */
@DIModule
public class ConditionsModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(ConditionChecker.class).to(ConditionCheckerImpl.class);
		bind(ConditionEvaluatorStrategy.class).to(MVELConditionEvaluatorStrategy.class);
	}
}
