package es.com.aaron.tcomms.platform.di_modules;

import com.google.inject.AbstractModule;
import es.com.aaron.tcomms.core.provisioning.DataProvisionService;
import es.com.aaron.tcomms.core.provisioning.DataProvisionServiceImpl;
import es.com.aaron.tcomms.core.provisioning.resolvers.ParametersResolverStrategy;
import es.com.aaron.tcomms.core.provisioning.resolvers.SimplePrefixedParametersResolver;
import es.com.aaron.tcomms.platform.bootstrap.DIModule;

/**
 * DataProvisionModule class.
 *
 * Class used to configure dependency injection by Guice.
 */
@DIModule
public class DataProvisionModule extends AbstractModule {
	@Override
	protected void configure() {
		bind(DataProvisionService.class).to(DataProvisionServiceImpl.class);
		bind(ParametersResolverStrategy.class).to(SimplePrefixedParametersResolver.class);
	}
}
