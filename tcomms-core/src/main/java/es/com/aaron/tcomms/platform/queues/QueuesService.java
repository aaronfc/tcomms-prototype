package es.com.aaron.tcomms.platform.queues;

/**
 * QueuesService interface.
 *
 * Used to enqueue java objects into specific queues by name.
 */
public interface QueuesService {

	public boolean enqueue(String queueName, Object ... objects);

	public <T> T dequeue(String queueName, Class<T> clazz);
}
