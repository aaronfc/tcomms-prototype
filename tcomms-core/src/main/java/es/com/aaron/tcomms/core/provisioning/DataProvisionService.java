package es.com.aaron.tcomms.core.provisioning;

import es.com.aaron.tcomms.core.exceptions.BrokenDependenciesException;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import java.util.Set;

/**
 * DataProvisionService interface.
 */
public interface DataProvisionService {

	/**
	 * Get a parameters bag with *at least* the required parameter keys specified. Starting from the input parameters
	 * given.
	 * @param inputParams Input parameters
	 * @param requireParamsKeySet Minimum set of parameter keys required
	 * @return ParamsBag generated
	 * @throws BrokenDependenciesException
	 */
	public ParamsBag getParameters(ParamsBag inputParams, Set<String> requireParamsKeySet) throws BrokenDependenciesException;
}
