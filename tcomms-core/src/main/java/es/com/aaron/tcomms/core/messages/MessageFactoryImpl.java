package es.com.aaron.tcomms.core.messages;

import es.com.aaron.tcomms.core.events.Event;
import es.com.aaron.tcomms.core.models.Communication;

/**
 * Implementation of a MessageFactory.
 */
public class MessageFactoryImpl implements MessageFactory {
    @Override
    public Message create(Event event, Communication communication) {
        return new Message(event, communication);
    }
}
