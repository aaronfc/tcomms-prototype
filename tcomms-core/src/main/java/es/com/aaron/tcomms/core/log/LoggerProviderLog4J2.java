package es.com.aaron.tcomms.core.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * LoggerProvider implementation for Log4J2.
 */
public class LoggerProviderLog4J2 implements LoggerProvider {
    @Override
    public Logger getLogger(Class clazz) {
        return LogManager.getLogger(clazz);
    }
}
