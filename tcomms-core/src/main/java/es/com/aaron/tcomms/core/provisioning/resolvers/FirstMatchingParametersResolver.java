package es.com.aaron.tcomms.core.provisioning.resolvers;

import es.com.aaron.tcomms.interfaces.data_providers.DataProvider;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import java.util.*;

/**
 * DRAFT of a parameters resolver that is not deterministic. It is not removed yet only to analyze its behaviour.
 * @deprecated
 */
public class FirstMatchingParametersResolver implements ParametersResolverStrategy {

	@Override
	public ParamsBag resolveParameters(ParamsBag inputParams, Set<DataProvider> providers, Set<String> required) {
		ParamsBag output = new ParamsBag();
		// Add all input parameters, they are always expected on the output
		output.putAll(inputParams);
		// Get data providers from paramskeyset
		Map<DataProvider, Set<String>> paramsByDataProvider = this.analyzeParameterKeys(providers, required);

		Dependencies dependencies = new Dependencies();

		List<DataProvider> unchecked = new ArrayList<DataProvider>();
		for (DataProvider dataProvider : paramsByDataProvider.keySet()) {
			if (dataProvider != null) {
				unchecked.add(dataProvider);
			}
		}
		Set<DataProvider> seen = new HashSet<DataProvider>();
		while(unchecked.size()>0) {
			DataProvider dataProvider = unchecked.remove(0); // Get first
			// Calculate needed parameters not present in input
			Set<String> missing = new HashSet<String>(dataProvider.getInputParameterKeys());
			missing.removeAll(inputParams.keySet()); // TODO Input or Output ?
			if (missing.size() > 0) {
				for (String missingKey : missing) {
					DataProvider found = this.findFirst(providers, missingKey);
					if (found == null) {
						// TODO Handle this... parameter not found! :(
						throw new RuntimeException("Parameter " + missingKey + " not found in any data provider!");
					} else {
						if (!paramsByDataProvider.containsKey(found)) {
							paramsByDataProvider.put(found, new HashSet<String>());
						}
						paramsByDataProvider.get(found).add(missingKey);
						dependencies.add(dataProvider, missingKey, found, missingKey);
						if (!unchecked.contains(found) && !seen.contains(found)) {
							unchecked.add(found);
						}
					}
				}
			}
			seen.add(dataProvider);
		}

		List<DataProvider> unresolved = new ArrayList<DataProvider>();
		seen = new HashSet<DataProvider>();
		Set<DataProvider> resolved = new HashSet<DataProvider>();
		for (DataProvider dataProvider : paramsByDataProvider.keySet()) {
			// TODO Remove this != null check ?
			if (dataProvider != null) {
				unresolved.add(dataProvider);
			}
		}
		while(unresolved.size()>0) {
			ParamsBag input = new ParamsBag(inputParams);
			DataProvider dataProvider = unresolved.remove(0); // Get first
			if (!dependencies.isDependent(dataProvider)) {
				output.put(dataProvider.getPrefix(),
						dataProvider.getRemoteParameters(input, paramsByDataProvider.get(dataProvider)));
				resolved.add(dataProvider);
			} else {
				Map<String, ParamReference> referencesMap = dependencies.getDependencies(dataProvider);
				Set<DataProvider> referencedDataProviders = new HashSet<DataProvider>();
				for (Map.Entry<String, ParamReference> entry : referencesMap.entrySet()) {
					String param = entry.getKey();
					ParamReference paramReference = entry.getValue();
					referencedDataProviders.add(paramReference.provider);
					if (resolved.contains(dataProvider)) {
						ParamsBag providerParams = (ParamsBag) output.get(paramReference.provider.getPrefix());
						input.put(param, providerParams.get(paramReference.input));
					}
				}
				if (resolved.containsAll(referencedDataProviders)) {
					output.put(dataProvider.getPrefix(),
							dataProvider.getRemoteParameters(input, paramsByDataProvider.get(dataProvider)));
					resolved.add(dataProvider);
				} else {
					if (!seen.containsAll(referencedDataProviders)) {
						unresolved.add(dataProvider);
					}
				}
			}
			seen.add(dataProvider);
		}

		// With inputparams check all required dataproviders
		// Set input parameters
		return output;
	}

	public Map<DataProvider, Set<String>> analyzeParameterKeys(Set<DataProvider> dataProviders, Set<String> paramKeys) {
		Map<DataProvider, Set<String>> output = new HashMap<DataProvider, Set<String>>();
		// Null key for unresolved
		output.put(null, new HashSet<String>());
		Map<String, DataProvider> prefixMap = new HashMap<String, DataProvider>();
		for (DataProvider dataProvider : dataProviders) {
			prefixMap.put(dataProvider.getPrefix(), dataProvider);
		}
		for (String paramKey : paramKeys) {
			String[] parts = paramKey.split("\\.", 2);
			if (parts.length == 2) {
				if (prefixMap.containsKey(parts[0])) {
					// We do not check if DataProvider can generate the parameter
					DataProvider dataProvider = prefixMap.get(parts[0]);
					if (!output.containsKey(dataProvider)) {
						output.put(dataProvider, new HashSet<String>());
					}
					output.get(dataProvider).add(parts[1]);
				} else {
					// Prefix does not match any data provider's
					output.get(null).add(paramKey);
				}
			} else {
				// Not prefixed
				output.get(null).add(paramKey);
			}
		}
		return output;
	}

	private DataProvider findFirst(Set<DataProvider> providers, String key) {
		for (DataProvider provider : providers) {
			if (provider.getAllParametersKeys().contains(key)) {
				return provider;
			}
		}
		return null;
	}

	private class ParamReference {
		private DataProvider provider;
		private String input;

		private ParamReference(DataProvider provider, String input) {
			this.provider = provider;
			this.input = input;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			ParamReference that = (ParamReference) o;

			if (!input.equals(that.input)) return false;
			if (!provider.equals(that.provider)) return false;

			return true;
		}

		@Override
		public int hashCode() {
			int result = provider.hashCode();
			result = 31 * result + input.hashCode();
			return result;
		}
	}

	private class Dependencies {
		Map<DataProvider, Map<String, ParamReference>> dependencies = new HashMap<DataProvider, Map<String, ParamReference>>();

		public void add(DataProvider dataProvider, String param, DataProvider dataProvider2, String param2) {
			ParamReference reference = new ParamReference(dataProvider2, param2);
			if (!dependencies.containsKey(dataProvider)) {
				dependencies.put(dataProvider, new HashMap<String, ParamReference>());
			}
			dependencies.get(dataProvider).put(param, reference);
		}

		public boolean isDependent(DataProvider dataProvider) {
			return dependencies.containsKey(dataProvider);
		}

		public Map<String, ParamReference> getDependencies(DataProvider dataProvider) {
			return dependencies.get(dataProvider);
		}
	}
}