package es.com.aaron.tcomms.platform.persistence;

import java.io.Serializable;

/**
 * AbstractEntity class.
 */
public abstract class AbstractEntity <P> implements Serializable {
	public abstract P getId();
}
