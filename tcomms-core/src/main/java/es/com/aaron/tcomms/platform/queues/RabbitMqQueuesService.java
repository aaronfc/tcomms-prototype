package es.com.aaron.tcomms.platform.queues;

import com.google.inject.Inject;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.platform.config.QueuesServiceConfig;
import org.apache.logging.log4j.Logger;

import java.io.*;

/**
 * RabbitMQ implementation of QueuesService interface.
 *
 * TODO Feature under development. Not tested.
 */
public class RabbitMqQueuesService implements QueuesService {

	Channel channel = null;
	Logger logger;
	private QueuesServiceConfig config;

	@Inject
	public RabbitMqQueuesService(LoggerProvider loggerProvider, QueuesServiceConfig config) {
		this.config = config;
		logger = loggerProvider.getLogger(getClass());
	}

	@Override
	public boolean enqueue(String queueName, Object ... objects) {
		try {
			Channel channel = getChannel();
			channel.queueDeclare(queueName, false, false, false, null);
			channel.basicPublish("", queueName, null, serialize(objects));
			return true;
		} catch (IOException e) {
			logger.error("Could not connect to RabbitMQ server.", e);
		}
		return false;
	}

	@Override
	public <T> T dequeue(String queueName, Class<T> clazz) {
		try {
			Channel channel = getChannel();
			channel.queueDeclare(queueName, false, false, false, null);
			QueueingConsumer consumer = new QueueingConsumer(channel);
			channel.basicConsume(queueName, true, consumer);
			QueueingConsumer.Delivery delivery = consumer.nextDelivery();
			return (T) deserialize(delivery.getBody());
		} catch (IOException e) {
			logger.error("Could not connect to RabbitMQ server.", e);
		} catch (InterruptedException e) {
			logger.error("Interrupted while waiting nextDelivery from RabbitMQ.", e);
		} catch (ClassNotFoundException e) {
			logger.error("Class not found when deserializing byteArray from RabbitMQ.", e);
		}
		return null;
	}

	public Channel getChannel() throws IOException{
		if (channel == null) {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(config.getHost());
			Connection connection = factory.newConnection();
			channel = connection.createChannel();
		}
		return channel;
	}

	private byte[] serialize(Object object) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		byte[] output = null;
		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(object);
			output = bos.toByteArray();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException ex) {
				// ignore close exception
			}
			try {
				bos.close();
			} catch (IOException ex) {
				// ignore close exception
			}
		}
		return output;
	}

	private Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
		ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
		ObjectInput in = null;
		try {
			in = new ObjectInputStream(bis);
			return in.readObject();
		} finally {
			try {
				bis.close();
			} catch (IOException ex) {
				// ignore close exception
			}
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				// ignore close exception
			}
		}
	}
}
