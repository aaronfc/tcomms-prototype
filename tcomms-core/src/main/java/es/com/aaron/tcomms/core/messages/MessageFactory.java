package es.com.aaron.tcomms.core.messages;

import es.com.aaron.tcomms.core.events.Event;
import es.com.aaron.tcomms.core.models.Communication;

/**
 * Message factory interface.
 */
public interface MessageFactory {
	/**
	 * Create a Message from an Event and a Communication.
	 * @param event
	 * @param communication
	 * @return
	 */
    public Message create(Event event, Communication communication);
}
