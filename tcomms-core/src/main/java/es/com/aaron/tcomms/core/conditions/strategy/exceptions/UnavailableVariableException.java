package es.com.aaron.tcomms.core.conditions.strategy.exceptions;

/**
 * UnavailableVariableException.
 * Thrown when a variable was not found and expected.
 */
public class UnavailableVariableException extends Exception {
	public UnavailableVariableException(String var) {
		super("Unavailable variable: \"" + var + "\"");
	}
}
