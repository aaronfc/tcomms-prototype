package es.com.aaron.tcomms.platform.di_modules;

import com.google.inject.AbstractModule;
import es.com.aaron.tcomms.platform.bootstrap.DIModule;
import es.com.aaron.tcomms.platform.config.QueuesServiceConfig;
import es.com.aaron.tcomms.platform.config.QueuesServiceConfigImpl;
import es.com.aaron.tcomms.platform.queues.QueuesService;
import es.com.aaron.tcomms.platform.queues.RabbitMqQueuesService;

/**
 * QueuesModule DI module class.
 *
 * Class used to configure dependency injection by Guice.
 */
@DIModule
public class QueuesModule extends AbstractModule {
	@Override
	protected void configure() {
		bind(QueuesService.class).to(RabbitMqQueuesService.class);
		bind(QueuesServiceConfig.class).to(QueuesServiceConfigImpl.class);
	}
}
