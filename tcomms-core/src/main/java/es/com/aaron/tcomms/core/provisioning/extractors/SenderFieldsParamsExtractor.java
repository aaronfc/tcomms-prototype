package es.com.aaron.tcomms.core.provisioning.extractors;

import com.google.inject.Inject;
import es.com.aaron.tcomms.interfaces.senders.SenderFields;
import es.com.aaron.tcomms.core.templates.strategy.TemplatingStrategy;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * SenderFields parameters extractor.
 *
 * This is the implementation of a ParamsExtractor that is supposed to be used when extracting the parameters
 * for an array of SenderFields in order to be correctly rendered.
 */
public class SenderFieldsParamsExtractor implements ParamsExtractor {

	TemplatingStrategy strategy;

	@Inject
	public SenderFieldsParamsExtractor(TemplatingStrategy strategy) {
		this.strategy = strategy;
	}

	@Override
	public Set<String> extract(Object... inputObjects) {
		Set<String> keySet = new HashSet<String>();
		for (Object inputObject : inputObjects) {
			if (inputObject instanceof SenderFields) {
				SenderFields senderFields = (SenderFields) inputObject;
				for (Map.Entry<String, String> entry : senderFields.entrySet()) {
                    Set<String> entryParamsKeySet = this.strategy.parse(entry.getValue());
                    keySet.addAll(entryParamsKeySet);
				}
			}
		}
		return keySet;
	}
}
