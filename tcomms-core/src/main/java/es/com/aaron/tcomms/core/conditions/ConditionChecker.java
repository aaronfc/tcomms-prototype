package es.com.aaron.tcomms.core.conditions;

import es.com.aaron.tcomms.core.conditions.exceptions.ConditionsEvaluationException;
import es.com.aaron.tcomms.core.models.Condition;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import java.util.Set;

/**
 * ConditionChecker interface.
 * This is the interface for a ConditionChecker that will handle the evaluation of a set of conditions and return
 * the final decision based on each of them.
 */
public interface ConditionChecker {
	/**
	 * This method checks if a given communication for a given set of parameters
	 * meets all the required conditions.
	 * @param conditions Set of conditions to check
	 * @param params Set of params
	 * @return Whether the communication is allowed or not
	 */
	public boolean evaluate(Set<Condition> conditions, ParamsBag params) throws ConditionsEvaluationException;
}