package es.com.aaron.tcomms.core.conditions.strategy;

import es.com.aaron.tcomms.core.conditions.strategy.exceptions.UnavailableVariableException;
import es.com.aaron.tcomms.interfaces.parameters.ParamsBag;

import java.util.Set;

/**
 * Conditions evaluator strategy interface.
 * This strategy is responsible of evaluating a given condition as a String, with a given bag of parameters.
 */
public interface ConditionEvaluatorStrategy {
	/**
	 * Check if a condition is evaluated to TRUE or to FALSE. Throws UnavailableVariableException when a variable is
	 * expected but not found in the given ParamsBag.
	 * @param string Condition as a string
	 * @param params Parameters bag
	 * @return Result of the condition evaluation
	 * @throws UnavailableVariableException
	 */
	public Boolean check(String string, ParamsBag params) throws UnavailableVariableException;

	/**
	 * This will parse a string as a condition and return a set of strings that define the key of the parameters
	 * expected to evaluate the condition.
	 * @param string Condition as a string
	 * @return Set of parameter keys
	 */
	public Set<String> parse(String string);
}
