package es.com.aaron.tcomms.core.notifications;

import com.google.inject.Inject;
import com.google.inject.Injector;
import es.com.aaron.tcomms.core.events.Event;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.core.messages.Message;
import es.com.aaron.tcomms.platform.config.MainProperties;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**
 * ActionsObserverService implementation.
 *
 * This class is in charge of propagating all action notifications to all the available observers.
 */
public class NotificationsServiceImpl implements NotificationsService {

	private Logger logger;
	private Injector injector;
	private MainProperties mainProperties;
	private Set<ActionsObserver> observers;

	@Inject
	public NotificationsServiceImpl(Injector injector, MainProperties mainProperties, LoggerProvider loggerProvider) {
		this.injector = injector;
		this.mainProperties = mainProperties;
		this.logger = loggerProvider.getLogger(getClass());
		this.observers = getActionsObservers();
	}

	@Override
	public void notifyEventAction(Event event, EventAction action) {
		for(ActionsObserver observer : observers) {
			try {
				observer.onEventAction(event, action);
			} catch (Exception e) {
				logger.error("Exception thrown when notifying the observer: " + observer.getClass().getName(), e);
			}
		}
	}

	@Override
	public void notifyEventException(Event event, Exception exception) {
		for(ActionsObserver observer : observers) {
			try {
				observer.onEventException(event, exception);
			} catch (Exception e) {
				logger.error("Exception thrown when notifying the observer: " + observer.getClass().getName(), e);
			}
		}
	}

	@Override
	public void notifyMessageAction(Message message, MessageAction action) {
		for(ActionsObserver observer : observers) {
			try {
				observer.onMessageAction(message, action);
			} catch (Exception e) {
				logger.error("Exception thrown when notifying the observer: " + observer.getClass().getName(), e);
			}
		}
	}

	@Override
	public void notifyMessageException(Message message, Exception exception) {
		for(ActionsObserver observer : observers) {
			try {
				observer.onMessageException(message, exception);
			} catch (Exception e) {
				logger.error("Exception thrown when notifying the observer: " + observer.getClass().getName(), e);
			}
		}
	}

	/**
	 * Get all ActionsObservers available.
	 * @return ActionsObservers available.
	 */
	private Set<ActionsObserver> getActionsObservers() {
		Set<ActionsObserver> set = new HashSet<ActionsObserver>();

		Boolean autodiscovery = Boolean.valueOf(mainProperties.getProperty("tcomms.observers.autodiscovery", "false"));
		if (autodiscovery) {
			// TODO This might be removed. Where to look for?
			logger.error("Auto-discovery for ActionsObservers is not implemented yet.");
		} else {
			// We use here Guice. External ActionsObservers should use Guice for injection or allow an empty constructor.
			String providersString = mainProperties.getProperty("tcomms.observers", "");
			String[] classes = providersString.split(",");
			for(String clazzString : classes) {
				try {
					Class clazz = Class.forName(clazzString.trim());
					set.add((ActionsObserver) injector.getInstance(clazz));
				} catch (ClassNotFoundException e) {
					logger.error(String.format("Could not instantiate data provider \"%s\". Class not found.", clazzString), e);
				}
			}
		}

		return set;
	}
}
