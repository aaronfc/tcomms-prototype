package es.com.aaron.tcomms.core.models.daos;

import es.com.aaron.tcomms.core.models.Condition;
import es.com.aaron.tcomms.platform.persistence.GenericDAOImpl;

/**
 * ConditionDAO class.
 */
public class ConditionDAO extends GenericDAOImpl<Condition, Long> {}
