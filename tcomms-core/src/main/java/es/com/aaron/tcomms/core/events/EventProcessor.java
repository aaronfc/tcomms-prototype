package es.com.aaron.tcomms.core.events;

/**
 * Event processor interface.
 */
public interface EventProcessor {
	/**
	 * Method to be callen when the processing of a event is triggered.
	 * @param event Event to process
	 */
	public void onEvent(Event event);
}
