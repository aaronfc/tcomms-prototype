package es.com.aaron.tcomms.platform.persistence;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * GenericDAO interface.
 */
public interface GenericDAO<E extends AbstractEntity, P> {
	/**
	 * Persist the indicated entity to database
	 * @param entity
	 * @return the primary key
	 */
	E insert(E entity);

	/**
	 * Retrieve an object using indicated ID
	 * @param id
	 * @return
	 */
	E find(P id);

	/**
	 * Update indicated entity to database
	 * @param entity
	 */
	E update(E entity);

	/**
	 * Delete indicated entity from database
	 * @param entity
	 */
	void delete(E entity);

	/**
	 * Return the entity class
	 * @return
	 */
	Class<E> getEntityClass();

	/**
	 * Get the entity manager
	 * @return
	 */
	EntityManager getEntityManager();

	/**
	 *
	 * @return
	 */
	List<E> findAll();
}
