package es.com.aaron.tcomms.core.log;

import org.apache.logging.log4j.Logger;

/**
 * LoggerProvider used as unique entry point for retrieving a logger.
 */
public interface LoggerProvider {
	/**
	 * Get logger for the given class.
	 * @param clazz Class the logger is requested for
	 * @return Logger
	 */
    public Logger getLogger(Class clazz);
}
