package es.com.aaron.tcomms.platform.di_modules;

import com.google.inject.AbstractModule;
import es.com.aaron.tcomms.core.messages.MessageFactory;
import es.com.aaron.tcomms.core.messages.MessageFactoryImpl;
import es.com.aaron.tcomms.core.messages.MessageProcessor;
import es.com.aaron.tcomms.core.messages.MessageProcessorImpl;
import es.com.aaron.tcomms.platform.bootstrap.DIModule;

/**
 * MessagesModule class.
 *
 * Class used to configure dependency injection by Guice.
 */
@DIModule
public class MessagesModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(MessageProcessor.class).to(MessageProcessorImpl.class);
		bind(MessageFactory.class).to(MessageFactoryImpl.class);
	}
}
