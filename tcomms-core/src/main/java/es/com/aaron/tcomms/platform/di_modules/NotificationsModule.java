package es.com.aaron.tcomms.platform.di_modules;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import es.com.aaron.tcomms.core.notifications.ActionsObserver;
import es.com.aaron.tcomms.core.notifications.NotificationsService;
import es.com.aaron.tcomms.core.notifications.NotificationsServiceImpl;
import es.com.aaron.tcomms.core.notifications.observers.LoggerActionsObserver;
import es.com.aaron.tcomms.platform.bootstrap.DIModule;

/**
 * NotificationsModule DI module class.
 *
 * Class used to configure dependency injection by Guice.
 */
@DIModule
public class NotificationsModule extends AbstractModule {
	@Override
	protected void configure() {
		bind(NotificationsService.class).to(NotificationsServiceImpl.class);
		// Multibind
		Multibinder.newSetBinder(binder(), ActionsObserver.class).addBinding().to(LoggerActionsObserver.class);
	}
}
