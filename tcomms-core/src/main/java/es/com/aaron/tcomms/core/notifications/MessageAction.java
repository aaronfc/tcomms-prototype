package es.com.aaron.tcomms.core.notifications;

/**
 * Actions available to be logged for Messages.
 */
public enum MessageAction {
	STARTED,
	CREATE,
	RENDERING_PARAMS_AVAILABLE,
	RENDERED_FIELDS_AVAILABLE,
	COMPLETED
}
