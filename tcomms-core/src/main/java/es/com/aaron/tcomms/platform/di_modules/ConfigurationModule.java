package es.com.aaron.tcomms.platform.di_modules;

import com.google.inject.AbstractModule;
import es.com.aaron.tcomms.platform.bootstrap.DIModule;
import es.com.aaron.tcomms.platform.config.MainProperties;
import es.com.aaron.tcomms.platform.config.MainPropertiesImpl;

/**
 * ConfigurationModule class.
 *
 * Class used to configure dependency injection by Guice.
 */
@DIModule
public class ConfigurationModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(MainProperties.class).to(MainPropertiesImpl.class).asEagerSingleton();
	}
}
