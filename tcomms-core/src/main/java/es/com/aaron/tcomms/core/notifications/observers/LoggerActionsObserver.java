package es.com.aaron.tcomms.core.notifications.observers;

import com.google.inject.Inject;
import es.com.aaron.tcomms.core.events.Event;
import es.com.aaron.tcomms.core.log.LoggerProvider;
import es.com.aaron.tcomms.core.messages.Message;
import es.com.aaron.tcomms.core.notifications.ActionsObserver;
import es.com.aaron.tcomms.core.notifications.EventAction;
import es.com.aaron.tcomms.core.notifications.MessageAction;
import org.apache.logging.log4j.Logger;

/**
 * LoggerObserver.
 *
 * This observer will only print all the data using the default logger.
 */
public class LoggerActionsObserver implements ActionsObserver {

	private Logger logger;

	@Inject
	public LoggerActionsObserver(LoggerProvider loggerProvider) {
		logger = loggerProvider.getLogger(getClass());
	}

	@Override
	public void onEventAction(Event event, EventAction action) {
		logger.debug(String.format("Action: %s - For event: %s", action.name(), event));
	}

	@Override
	public void onEventException(Event event, Exception e) {
		logger.error(String.format("Exception! - For event: %s", event), e);
	}

	@Override
	public void onMessageAction(Message message, MessageAction action) {
		logger.debug(String.format("Action: %s - For message: %s", action.name(), message));
	}

	@Override
	public void onMessageException(Message message, Exception e) {
		logger.error(String.format("Exception! - For message: %s", message), e);
	}
}
