package es.com.aaron.tcomms.platform.persistence;

import com.google.inject.Inject;
import com.google.inject.TypeLiteral;
import com.google.inject.persist.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * GenericDAOImpl class.
 *
 * Most DAO operations are limited to CRUD ones. This abstract class encapsulates all of them in a generic way to
 * entities implementing AbstractEntity.
 */
public abstract class GenericDAOImpl<E extends AbstractEntity, P> implements GenericDAO<E, P> {

	@Inject
	private EntityManager entityManager;
	private Class<E> entityClass;

	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public E insert(E entity) {
		entityManager.persist(entity);
		return entity;
	}

	@Override
	public E find(P id) {
		return entityManager.find(getEntityClass(), id);
	}

	@Override
	@Transactional
	public E update(E entity) {
		return entityManager.merge(entity);
	}

	@Override
	@Transactional
	public void delete(E entity) {
		entityManager.remove(entity);
	}

	@SuppressWarnings("unchecked")
	public Class<E> getEntityClass() {
		if (entityClass == null) {
			//Type type = getClass().getGenericSuperclass();
			Type type = TypeLiteral.get(getClass()).getSupertype( GenericDAO.class ).getType();
			if (type instanceof ParameterizedType) {
				ParameterizedType paramType = (ParameterizedType) type;
				entityClass = (Class<E>) paramType.getActualTypeArguments()[0];
			} else {
				throw new IllegalArgumentException(
						"Could not guess entity class by reflection");
			}
		}
		return entityClass;
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public List<E> findAll() {
		return getQuery("GetAll").getResultList();
	}

	protected TypedQuery<E> getQuery(String queryName) {
		return this.entityManager.createNamedQuery(getEntityClass().getSimpleName()+"."+queryName, entityClass);
	}
}
