
    alter table `condition` 
        drop 
        foreign key FK_dl150wtykk40061cy4mqrxmst;

    alter table communication 
        drop 
        foreign key FK_5xk6k0ibs7v2wesgxppo236t4;

    drop table if exists `condition`;

    drop table if exists communication;

    drop table if exists sender_envelope;

    drop table if exists tinclude;

    create table `condition` (
        id bigint not null auto_increment,
        conditionString varchar(255),
        name varchar(255),
        communication_id bigint,
        primary key (id)
    );

    create table communication (
        id bigint not null auto_increment,
        description varchar(255),
        eventName varchar(255),
        title varchar(255),
        envelopeId bigint,
        primary key (id)
    );

    create table sender_envelope (
        id bigint not null auto_increment,
        data longblob,
        senderClass varchar(255),
        primary key (id)
    );

    create table tinclude (
        id bigint not null auto_increment,
        content varchar(255),
        title varchar(255),
        primary key (id)
    );

    alter table `condition` 
        add constraint FK_dl150wtykk40061cy4mqrxmst 
        foreign key (communication_id) 
        references communication (id);

    alter table communication 
        add constraint FK_5xk6k0ibs7v2wesgxppo236t4 
        foreign key (envelopeId) 
        references sender_envelope (id);
