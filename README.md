TComms
=========

TComms is a tool which only purpose is to give a quick solution for managing communications processing for your project.
It is composed by two main components, the tcomms-core and the tcomms-standalone modules which are described below.

Main features:

- Event-driven communications system. Fire an event with the required params to the TComms and forget.
- Multiple delivery methods available and extensible. (Email, SMS, Android Push Notifications, ...)
- Customizable data providers to elaborate the contents of your communications.
- Easy targeting the communications thanks to the conditions system.
- Advanced templating for your communications. Currently using [Velocity](https://github.com/apache/velocity-engine) with [TInclude](#TODO).
- Fully available logging interface by adding your own observers.

Version
----
1.0

Components definition
---------------------
### tcomms-core
It is in fact the **heart** of the tool (what a surprise!) and it can be used as a library if an advanced integration into your project is required.
Common users will only use tcomms-standalone component, and forget about the tcomms-core part.
Additional documentation can be found [here](#TODO).

### tcomms-standalone
This is an almost-out-of-the-box JAR package that only requires (thus the *almost* part) a minimal configuration files and a set of data providers.
After specifying your `persistence.xml`, generating the schemas for it and setting up the `tcomms.properties` file with your data providers you will be done.
Executing the jar will start up a servlet ready to hear delicious events and its parameters from you.
Additionally a REST API will be ready to administer the internals of the tool (setup new communications, conditions and templates).


MYSQL Schema generation:
-----------------------
Download your schema.sql from the `/schemas` folder depending on the database technology used.

    # Create tcomms:nanana user
    mysql -uroot -proot -e "CREATE USER 'tcomms'@'%' IDENTIFIED BY 'nanana';"
    # Create database
    mysql -uroot -proot -e "CREATE DATABASE tcommsdb_test;"
    # Grant privileges
    mysql -uroot -proot -e "GRANT all privileges ON tcommsdb_test.* TO 'tcomms'@'%';"
    # Load schema into new DB logger as new user
    mysql -utcomms -pnanana tcommsdb_test < schema.sql

More
---
WIP ...
    
